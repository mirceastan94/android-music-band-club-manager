package com.manager.client.utils

import android.app.Application
import android.content.Context

class AppContext : Application() {

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    companion object {
        var instance: AppContext? = null
            private set

        val context: Context?
            get() = instance

    }
}