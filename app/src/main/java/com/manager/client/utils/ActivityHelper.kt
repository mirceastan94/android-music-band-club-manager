package com.manager.client.utils

import android.app.Activity
import android.content.Context
import android.content.Intent

/**
 * This class includes utils methods for the activities within the app
 * @author Mircea Stan
 */
object ActivityHelper {

    /**
     * Starts the desired activity
     */
    fun startActivity(context: Context, clazz: Class<out Activity>) {
        val intent = Intent(context, clazz)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
    }

}