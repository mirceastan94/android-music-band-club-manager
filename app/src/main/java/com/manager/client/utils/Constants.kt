package com.manager.client.utils

import com.manager.client.R

public const val SELECTED_LANGUAGE = "Locale.Helper.Selected.Language"
// Emulator
// public const val restEndpoint = "http://10.0.2.2/80/"
// Desktop-PC
// public const val restEndpoint = "http://192.168.0.107:8080/"
// Cloud
public const val restEndpoint = "https://music-band-club-manager-rest.herokuapp.com:443/"
public const val JSON_WEB_TOKEN = "JSONWebToken"
public const val ADMIN = "admin"
public const val USER = "user"
public const val ADMIN_ROLE = "ROLE_ADMIN"
public const val USER_ROLE = "ROLE_USER"
public const val BAND = "band"
public const val BAND_JOB_OFFER = "band job offer"
public const val CLUB = "club"
public const val CLUB_JOB_OFFER = "club job offer"
public const val EVENT = "event"
public const val MASTER_USER = "master"
public const val BULLET_DOT = "• "
public const val COMMA = ", "
public const val LINE = "-"
public const val UNDERLINE = "_"
public const val h = "h"
public const val T = "T"
public const val BLANK = ""
public const val SPACE = " "
public const val ARROW_WITH_SPACE = "-> "
public const val ZERO = "0"
public const val QUOTES = "\""
public const val EN = "en"
public const val DE = "de"
public const val MASTER_USER_CHANGE_ROLE_CONSTRAINT = "super user must always have"
public const val USER_WITH_ADMIN_ROLE_DELETE_CONSTRAINT = "as it has an admin role"
public const val GMAIL_URL = "https://mail.google.com/mail/"

public fun mapMusicGenresToSongSamples(): HashMap<String, Int> {
    return hashMapOf(
        "Blues" to R.raw.sound_blues,
        "Classical" to R.raw.sound_classical,
        "Country" to R.raw.sound_country,
        "EDM" to R.raw.sound_edm,
        "Jazz" to R.raw.sound_jazz,
        "Latin" to R.raw.sound_latin,
        "Opera" to R.raw.sound_opera,
        "Pop" to R.raw.sound_pop,
        "R&B" to R.raw.sound_r_and_b,
        "Rap" to R.raw.sound_rap,
        "Reggae" to R.raw.sound_reggae,
        "Rock" to R.raw.sound_rock
    )
}