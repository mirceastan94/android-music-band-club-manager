package com.manager.client.utils

import android.content.Context
import android.media.MediaPlayer

/**
 * This class facilitates the media player integration within the app
 */
object MediaPlayerManager {

    private var mediaPlayer: MediaPlayer? = null

    fun play(context: Context, resId: Int) {
        play(context, resId, null, null)
    }

    fun play(context: Context, resId: Int, errorListener: MediaPlayer.OnErrorListener) {
        play(context, resId, null, errorListener)
    }

    fun play(
        context: Context,
        resId: Int,
        completionListener: MediaPlayer.OnCompletionListener? = null,
        errorListener: MediaPlayer.OnErrorListener? = null
    ) {
        if (mediaPlayer != null) {
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer!!.stop()
            }
            mediaPlayer!!.release()
            mediaPlayer = null
        }
        mediaPlayer = MediaPlayer.create(context.applicationContext, resId)
        mediaPlayer!!.setOnCompletionListener(completionListener)
        mediaPlayer!!.setOnErrorListener(errorListener)
        mediaPlayer!!.start()
    }

}