package com.manager.client.network

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.manager.client.network.utils.Provider
import com.manager.client.utils.AppContext.Companion.context
import com.manager.client.utils.JSON_WEB_TOKEN
import com.manager.client.utils.restEndpoint
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

lateinit var sharedPreferences: SharedPreferences
lateinit var jsonWebToken: String

object UnauthenticatedRetrofit : Provider<Retrofit>({
    makeRetrofit()!!
})

object JWTRetrofit : Provider<Retrofit>({
    makeRetrofit(accessTokenProvidingInterceptor())!!
})

fun makeRetrofit(vararg interceptors: Interceptor): Retrofit? {
    return Retrofit.Builder()
        .baseUrl(restEndpoint)
        .client(makeHttpClient(interceptors))
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

private fun makeHttpClient(interceptors: Array<out Interceptor>) = OkHttpClient.Builder()
    .connectTimeout(60, TimeUnit.SECONDS)
    .readTimeout(60, TimeUnit.SECONDS)
    .addInterceptor(headersInterceptor())
    .apply { interceptors().addAll(interceptors) }
    .build()

fun accessTokenProvidingInterceptor() = Interceptor { chain ->
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    jsonWebToken = sharedPreferences.getString(JSON_WEB_TOKEN, "")
    chain.proceed(
        chain.request().newBuilder()
            .addHeader(
                "Authorization",
                "Bearer $jsonWebToken"
            )
            .build()
    )
}

fun headersInterceptor() = Interceptor { chain ->
    chain.proceed(
        chain.request().newBuilder()
            .addHeader("Accept", "application/json")
            .addHeader("Accept-Language", "en")
            .addHeader("Content-Type", "application/json")
            .build()
    )
}