package com.manager.client.network

import com.manager.client.models.band.dto.*
import com.manager.client.models.club.dto.*
import com.manager.client.models.club.entities.ClubMemberSummary
import com.manager.client.models.events.dto.*
import com.manager.client.models.jobs.CreateJobOfferDTO
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.jobs.JobOfferListDTO
import com.manager.client.models.jobs.JobOfferSearchDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.main.NotificationDTO
import com.manager.client.models.main.NotificationListDTO
import com.manager.client.models.main.SendEmailDetailsDTO
import com.manager.client.models.users.dto.*
import retrofit2.Call
import retrofit2.http.*

interface ManagerAPI {

    // Admin role specific calls

    // <--- Bands --->
    @GET("manager/api/bands/all")
    fun getAllBands()
            : Call<BandListDTO>

    @DELETE("manager/api/bands/{id}")
    fun deleteBand(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--- Clubs --->
    @GET("manager/api/clubs/all")
    fun getAllClubs()
            : Call<ClubListDTO>

    @DELETE("manager/api/clubs/{id}")
    fun deleteClub(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--- Users --->
    @GET("manager/api/users/all")
    fun getAllUsers()
            : Call<UserListDTO>

    @GET("manager/api/users/{id}")
    fun getSelectedUserDetails(@Path("id") id: Long)
            : Call<UserDetailsDTO>

    @PUT("manager/api/users/{id}/role/{role}")
    fun editUserRole(@Path("id") id: Long, @Path("role") role: String)
            : Call<APIResponseDTO>

    @DELETE("manager/api/users/{id}")
    fun deleteUser(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--------------------------->

    // User role specific calls

    // <--- Bands --->
    @POST("manager/api/bands/create")
    fun createBand(@Body newBand: CreateBandDTO)
            : Call<APIResponseDTO>

    @GET("manager/api/bands/mine")
    fun getBand(): Call<BandDetailsDTO>

    @GET("manager/api/bands/details/{id}")
    fun getSelectedBandDetails(@Path("id") id: Long)
            : Call<BandDetailsDTO>

    @GET("manager/api/bands/{name}")
    fun getSelectedBandDetails(@Path("name") name: String)
            : Call<BandDetailsDTO>

    @PUT("manager/api/bands/mine")
    fun updateBand(@Body updateband: UpdateBandDTO)
            : Call<APIResponseDTO>

    @DELETE("manager/api/bands/mine")
    fun removeBand()
            : Call<APIResponseDTO>

    // <--- Band members --->
    @GET("manager/api/bands/{name}/members")
    fun getAllBandMembers(@Path("name") name: String)
            : Call<BandMemberListDTO>

    @GET("manager/api/bands/mine/members")
    fun getMyBandMembers()
            : Call<BandMemberListDTO>

    @GET("manager/api/users/band/free")
    fun getAllMembersWithoutBand()
            : Call<BandMemberListDTO>

    @GET("manager/api/bands/mine/members/without/owner")
    fun getAllBandMembersExceptTheCurrentOwner()
            : Call<BandMemberListDTO>

    @DELETE("manager/api/bands/delete/myself/from/band")
    fun resignFromBand()
            : Call<APIResponseDTO>

    @POST("manager/api/bands/mine/add/member/{id}")
    fun addBandMember(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @PUT("manager/api/bands/mine/change/owner/{id}")
    fun changeBandOwner(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @DELETE("manager/api/bands/mine/delete/member/{id}")
    fun deleteBandMember(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--------------------------->

    // <--- Clubs --->
    @POST("manager/api/clubs/create")
    fun createClub(@Body newBand: CreateClubDTO)
            : Call<APIResponseDTO>

    @GET("manager/api/clubs/mine")
    fun getClub(): Call<ClubDetailsDTO>

    @GET("manager/api/clubs/details/{id}")
    fun getSelectedClubDetails(@Path("id") id: Long)
            : Call<ClubDetailsDTO>

    @GET("manager/api/clubs/{name}")
    fun getSelectedClubDetails(@Path("name") name: String)
            : Call<ClubDetailsDTO>

    @PUT("manager/api/clubs/mine")
    fun updateClub(@Body updateClub: UpdateClubDTO)
            : Call<APIResponseDTO>

    @DELETE("manager/api/clubs/mine")
    fun removeClub()
            : Call<APIResponseDTO>

    // <--- Club members --->
    @GET("manager/api/clubs/{name}/members")
    fun getAllClubMembers(@Path("name") name: String)
            : Call<ClubMemberListDTO>

    @GET("manager/api/clubs/mine/members")
    fun getMyClubMembers()
            : Call<ClubMemberListDTO>

    @GET("manager/api/users/club/free")
    fun getAllMembersWithoutClub()
            : Call<ClubMemberListDTO>

    @GET("manager/api/clubs/mine/members/without/owner")
    fun getAllClubMembersExceptTheCurrentOwner()
            : Call<ClubMemberListDTO>

    @DELETE("manager/api/clubs/delete/myself/from/club")
    fun resignFromClub()
            : Call<APIResponseDTO>

    @POST("manager/api/clubs/mine/add/member/{id}")
    fun addClubMember(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @PUT("manager/api/clubs/mine/change/owner/{id}")
    fun changeClubOwner(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @DELETE("manager/api/clubs/mine/delete/member/{id}")
    fun deleteClubMember(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--------------------------->

    // <--- Job offers --->
    @GET("manager/api/job/offers/{category}/all")
    fun getAllJobOffersByCategory(@Path("category") category: String)
            : Call<JobOfferListDTO>

    @POST("manager/api/job/offers/custom")
    fun getCustomJobOffers(@Body jobOfferSearchDTO: JobOfferSearchDTO)
            : Call<JobOfferListDTO>

    @GET("manager/api/job/offers/{category}/mine")
    fun getAllCurrentJobOffersByCategoryAndUser(@Path("category") category: String)
            : Call<JobOfferListDTO>

    @GET("manager/api/job/offers/{id}")
    fun getSelectedJobOfferDetails(@Path("id") id: Long)
            : Call<JobOfferDetailsDTO>

    @POST("manager/api/job/offers/create")
    fun createJobOffer(@Body newJobOffer: CreateJobOfferDTO)
            : Call<APIResponseDTO>

    @DELETE("manager/api/job/offers/delete/{id}")
    fun deleteJobOffer(@Path("id") id: Long)
            : Call<APIResponseDTO>

    // <--- Events --->
    @POST("manager/api/events/create")
    fun createEvent(@Body newEvent: CreateEventDTO)
            : Call<APIResponseDTO>

    @GET("manager/api/events/all/future/unjoined")
    fun getAllUpcomingEvents()
            : Call<EventListDTO>

    @GET("manager/api/events/mine/past")
    fun getPastEvents()
            : Call<EventListDTO>

    @GET("manager/api/events/mine/future")
    fun getFutureEvents()
            : Call<EventListDTO>

    @GET("manager/api/events/myClub")
    fun getClubEvents()
            : Call<EventListDTO>

    @GET("manager/api/events/all")
    fun getAllEvents()
            : Call<EventListDTO>

    @POST("manager/api/events/custom/unjoined")
    fun getCustomCurrentEvents(@Body eventSearchDTO: EventSearchDTO)
            : Call<EventListDTO>

    @GET("manager/api/events/details/{id}")
    fun getSelectedEventDetails(@Path("id") id: Long)
            : Call<EventDetailsDTO>

    @DELETE("manager/api/events/delete/{id}")
    fun deleteEvent(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @POST("manager/api/events/add/participant/{id}")
    fun addEventParticipant(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @DELETE("manager/api/events/delete/participant/{id}")
    fun deleteEventParticipant(@Path("id") id: Long)
            : Call<APIResponseDTO>

    @GET("manager/api/events/{id}/participants")
    fun getEventParticipants(@Path("id") id: Long)
            : Call<EventParticipantListDTO>

    @POST("manager/api/events/add/review")
    fun createReview(@Body newReview: CreateReviewDTO)
            : Call<APIResponseDTO>

    @GET("manager/api/events/{id}/reviews")
    fun getEventFeedbackList(@Path("id") id: Long)
            : Call<ReviewListDTO>

    // <--- Email Service --->
    @POST("manager/api/emails/job/offer/sendEmail")
    fun sendEmail(@Body newEmail: SendEmailDetailsDTO)
            : Call<APIResponseDTO>

    // <--- Notifications --->
    @GET("manager/api/notifications/all")
    fun getNotifications()
            : Call<NotificationListDTO>

    @GET("manager/api/notifications/{id}")
    fun getSelectedNotificationDetails(@Path("id") id: Long)
            : Call<NotificationDTO>

    // <--- Users --->
    @GET("manager/api/users/me")
    fun getLoggedInUserDetails()
            : Call<UserDetailsDTO>

    @POST("manager/api/users/register")
    fun signUp(@Body newUser: RegisterUserDTO)
            : Call<APIResponseDTO>

    @POST("manager/api/users/login")
    fun logIn(@Body user: LoginUserDTO)
            : Call<JWTAuthResponseDTO>

    @PUT("manager/api/users/{id}")
    fun editUser(@Path("id") id: Long, @Body user: EditUserDTO)
            : Call<APIResponseDTO>

    @DELETE("manager/api/users/me")
    fun removeAccount()
            : Call<APIResponseDTO>
}