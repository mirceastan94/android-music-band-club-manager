package com.manager.client.ui.activities

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.general.ClubOverviewMembersAdapter
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_club_status.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the club status activity implementation
 */
class ClubStatusActivity : AppCompatActivity() {

    lateinit var clubOverviewMembersAdapter: ClubOverviewMembersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_status)

        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        statusClubNameText.setText(userClubDetails.name)
        statusClubLocationText.setText(userClubDetails.location)
        statusClubMusicGenreText.setText(userClubDetails.genre)
        statusClubRatingBar.setRating(userClubDetails.rating.toFloat())

        clubOverviewMembersAdapter = ClubOverviewMembersAdapter()

        statusClubMembersRecyclerView.layoutManager = LinearLayoutManager(this)
        statusClubMembersRecyclerView.adapter = clubOverviewMembersAdapter

        resignButton?.setOnClickListener {
            showResignFromClubDialog()
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

    /**
     * Displays the member resign dialog
     */
    fun showResignFromClubDialog() {
        MediaPlayerManager.play(this, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.club_status_resign_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    resignFromClub()
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

    /**
     * Deletes the member from the club
     */
    fun resignFromClub() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.resignFromClub()
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    ActivityHelper.startActivity(applicationContext, MainActivity::class.java)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubStatusActivity) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }
}

