package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.notifications.NotificationsAdapter
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_main.*

/**
 * This is the main activity implementation
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mainActivityNotificationsAdapter = NotificationsAdapter(applicationContext)

        generalFeedRecyclerView.layoutManager = LinearLayoutManager(this)
        generalFeedRecyclerView.adapter = mainActivityNotificationsAdapter

        bandImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, BandMenuActivity::class.java)
        }

        clubImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, ClubMenuActivity::class.java)
        }

        eventsImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, EventsMenuActivity::class.java)
        }

        accountImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, AccountActivity::class.java)
        }

        refreshImageView.setOnClickListener {
            mainActivityNotificationsAdapter = NotificationsAdapter(applicationContext)
            generalFeedRecyclerView.layoutManager = LinearLayoutManager(this)
            generalFeedRecyclerView.adapter = mainActivityNotificationsAdapter
            var refreshToast = Toast.makeText(this, R.string.notifications_refreshed_text, Toast.LENGTH_LONG)
            refreshToast.setGravity(Gravity.BOTTOM, 0, 40)
            refreshToast.show()
            MediaPlayerManager.play(this, R.raw.sound_warning)
        }

        aboutImageView.setOnClickListener {
            ActivityHelper.startActivity(this, AboutAppActivity::class.java)
        }

        MediaPlayerManager.play(this, R.raw.sound_main)
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

