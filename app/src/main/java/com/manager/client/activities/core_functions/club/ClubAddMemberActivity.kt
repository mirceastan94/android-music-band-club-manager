package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubAddMemberAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_club_add_member.*

/**
 * This is the club add member activity implementation
 */
class ClubAddMemberActivity : AppCompatActivity() {

    lateinit var addMemberDashboardAdapter: ClubAddMemberAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_add_member)

        addMemberDashboardAdapter = ClubAddMemberAdapter()

        membersWithoutClubRecyclerView.layoutManager = LinearLayoutManager(this)
        membersWithoutClubRecyclerView.adapter = addMemberDashboardAdapter

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

