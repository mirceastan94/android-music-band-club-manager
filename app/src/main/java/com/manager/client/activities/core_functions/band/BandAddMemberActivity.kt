package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.tasks.BandAddMemberAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_band_add_member.*

/**
 * This is the band add member activity implementation
 */
class BandAddMemberActivity : AppCompatActivity() {

    lateinit var addMemberDashboardAdapter: BandAddMemberAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_add_member)

        addMemberDashboardAdapter = BandAddMemberAdapter()

        membersWithoutBandRecyclerView.layoutManager = LinearLayoutManager(this)
        membersWithoutBandRecyclerView.adapter = addMemberDashboardAdapter

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

