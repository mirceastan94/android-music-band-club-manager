package com.manager.client.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.manager.client.R
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager

const val SPLASH_DELAY: Long = 10000

/**
 * This is the splash screen activity which pops up the first time a user starts the app
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // loads the SharedPreferences to check if this is the first app run on the user's Smartphone
        val prefs = getSharedPreferences("prefs", 0)
        val firstRun = prefs.getBoolean("firstRun", true)

        // checks if this is the first run after install to display this Splash activity
        if (firstRun) {
            setContentView(R.layout.activity_splash)
            MediaPlayerManager.play(this, R.raw.sound_splash)
            // saves the updated SharedPreferences for future uses
            val editor = prefs.edit()
            editor.putBoolean("firstRun", false)
            editor.commit()
        } else {
            // loads the login Activity directly
            ActivityHelper.startActivity(this, LoginActivity::class.java)
            finish()
        }

        // saves the chosen language and starts the Login activity
        val englishLocale = findViewById(R.id.englishLanguageImageView) as? ImageView
        val germanLocale = findViewById(R.id.germanLanguageImageView) as? ImageView

        englishLocale?.setOnClickListener {
            LocaleHelper.saveLanguagePreference(this, "en")
            ActivityHelper.startActivity(this, LoginActivity::class.java)
            finish()
        }

        germanLocale?.setOnClickListener {
            LocaleHelper.saveLanguagePreference(this, "de")
            ActivityHelper.startActivity(this, LoginActivity::class.java)
            finish()
        }

    }

}