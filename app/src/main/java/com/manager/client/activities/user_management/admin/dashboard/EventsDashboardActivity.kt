package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.user_management.admin.EventsDashboardAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_dashboard_events.*

/**
 * This is the events dashboard implementation
 */
class EventsDashboardActivity : AppCompatActivity() {

    lateinit var eventDashBoardAdapter: EventsDashboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_events)

        eventDashBoardAdapter = EventsDashboardAdapter()

        eventsRecycleView.layoutManager = LinearLayoutManager(this)
        eventsRecycleView.adapter = eventDashBoardAdapter


    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

