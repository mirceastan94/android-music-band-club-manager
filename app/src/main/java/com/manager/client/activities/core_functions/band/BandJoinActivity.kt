package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.general.BandJoinJobOffersAdapter
import com.manager.client.models.jobs.JobOfferSearchDTO
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_band_join.*

/**
 * This is the band join activity implementation
 */
class BandJoinActivity : AppCompatActivity() {

    var bandName: String = ""
    var location: String = ""
    var profession: String = ""
    var experience: String = ""
    var bandJobOfferSearchDTO: JobOfferSearchDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_join)

        val bandJobOffersAdapter =
            BandJoinJobOffersAdapter(bandJobOfferSearchDTO, applicationContext)

        bandJobOffersRecyclerView.layoutManager = LinearLayoutManager(this)
        bandJobOffersRecyclerView.adapter = bandJobOffersAdapter

        // sets the profession spinner adapter
        val professionSpinnerAdapter =
            ArrayAdapter.createFromResource(this, R.array.user_profession, R.layout.spinner_item)
        professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        bandJobOfferProfessionSpinner.adapter = professionSpinnerAdapter
        bandJobOfferProfessionSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        // sets the experience spinner adapter
        val experienceSpinnerAdapter =
            ArrayAdapter.createFromResource(this, R.array.user_experience, R.layout.spinner_item)
        experienceSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        bandJobOfferExperienceSpinner.adapter = experienceSpinnerAdapter
        bandJobOfferExperienceSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        // improves user experience depending on which checkbox(es) is/are selected
        manageCheckboxes()

        // updates the band job offers list using the specified criteria
        updateBandJobOfferButton.setOnClickListener {
            if (bandJobOfferDeselectAllCheckboxes.isChecked) {
                Toast.makeText(
                    this,
                    R.string.band_job_offer_no_criteria_chosen_error_text,
                    Toast.LENGTH_LONG
                ).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            } else {
                if (validateParameters()) {
                    // sends the validated request
                    bandJobOfferSearchDTO =
                        JobOfferSearchDTO(BAND, bandName, BLANK, profession, location, experience)
                    val bandJobOffersAdapter =
                        BandJoinJobOffersAdapter(bandJobOfferSearchDTO, applicationContext)

                    bandJobOffersRecyclerView.layoutManager = LinearLayoutManager(this)
                    bandJobOffersRecyclerView.adapter = bandJobOffersAdapter
                }
            }
        }

    }

    private fun manageCheckboxes() {
        // selects all checkboxes
        bandJobOfferSelectAllCheckboxes.setOnClickListener {
            bandJobOfferNameCheckBox.setChecked(true)
            bandJobOfferLocationCheckBox.setChecked(true)
            bandJobOfferProfessionCheckBox.setChecked(true)
            bandJobOfferExperienceCheckBox.setChecked(true)
            bandJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        // deselects all checkboxes
        bandJobOfferDeselectAllCheckboxes.setOnClickListener {
            bandJobOfferNameCheckBox.setChecked(false)
            bandJobOfferLocationCheckBox.setChecked(false)
            bandJobOfferProfessionCheckBox.setChecked(false)
            bandJobOfferExperienceCheckBox.setChecked(false)
            bandJobOfferSelectAllCheckboxes.setChecked(false)
        }

        // deselects "All checkboxes" since neither none nor all of them are checked
        bandJobOfferNameCheckBox.setOnClickListener {
            bandJobOfferSelectAllCheckboxes.setChecked(false)
            bandJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        bandJobOfferLocationCheckBox.setOnClickListener {
            bandJobOfferSelectAllCheckboxes.setChecked(false)
            bandJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        bandJobOfferProfessionCheckBox.setOnClickListener {
            bandJobOfferSelectAllCheckboxes.setChecked(false)
            bandJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        bandJobOfferExperienceCheckBox.setOnClickListener {
            bandJobOfferSelectAllCheckboxes.setChecked(false)
            bandJobOfferDeselectAllCheckboxes.setChecked(false)
        }
    }

    private fun validateParameters(): Boolean {
        var validCriteria = true

        if (bandJobOfferNameCheckBox.isChecked) {
            if (validateBandJobOfferName()) {
                bandName = bandJobOfferNameText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.band_name_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (bandJobOfferLocationCheckBox.isChecked) {
            if (validateBandJobOfferLocation()) {
                location = bandJobOfferLocationText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.band_job_offer_location_error_text, Toast.LENGTH_LONG)
                    .show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (bandJobOfferProfessionCheckBox.isChecked) {
            profession = bandJobOfferProfessionSpinner.selectedItem.toString()
        }

        if (bandJobOfferExperienceCheckBox.isChecked) {
            experience = bandJobOfferExperienceSpinner.selectedItem.toString()
        }
        return validCriteria
    }

    /**
     * Validates the name input before searching specific bands which include this criteria
     */
    fun validateBandJobOfferName(): Boolean {
        var validBandName = true

        val bandJobOfferNameTextValue = bandJobOfferNameText!!.text.toString()

        if (bandJobOfferNameTextValue.isEmpty() || bandJobOfferNameTextValue.length < 5) {
            bandJobOfferNameText!!.error = resources.getString(R.string.band_name_error_text)
        } else {
            bandJobOfferNameText!!.error = null
        }

        return validBandName
    }

    /**
     * Validates the location input before searching specific bands which include this criteria
     */
    fun validateBandJobOfferLocation(): Boolean {
        var validUserData = true

        val bandJobOfferLocationTextValue = bandJobOfferLocationText!!.text.toString()

        if (bandJobOfferLocationTextValue.isEmpty()) {
            bandJobOfferLocationText!!.error =
                resources.getString(R.string.band_location_error_text)
            validUserData = false
        } else {
            bandJobOfferLocationText!!.error = null
        }

        return validUserData
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

