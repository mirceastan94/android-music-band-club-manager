package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.general.ClubJoinJobOffersAdapter
import com.manager.client.models.jobs.JobOfferSearchDTO
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_club_join.*

/**
 * This is the club join activity implementation
 */
class ClubJoinActivity : AppCompatActivity() {

    var clubName: String = ""
    var location: String = ""
    var profession: String = ""
    var experience: String = ""
    var clubJobOfferSearchDTO: JobOfferSearchDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_join)

        val clubJobOffersAdapter =
            ClubJoinJobOffersAdapter(clubJobOfferSearchDTO, applicationContext)

        clubJobOffersRecyclerView.layoutManager = LinearLayoutManager(this)
        clubJobOffersRecyclerView.adapter = clubJobOffersAdapter

        // sets the profession spinner adapter
        val professionSpinnerAdapter =
            ArrayAdapter.createFromResource(this, R.array.club_profession, R.layout.spinner_item)
        professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        clubJobOfferProfessionSpinner.adapter = professionSpinnerAdapter
        clubJobOfferProfessionSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        // sets the experience spinner adapter
        val experienceSpinnerAdapter =
            ArrayAdapter.createFromResource(this, R.array.user_experience, R.layout.spinner_item)
        experienceSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        clubJobOfferExperienceSpinner.adapter = experienceSpinnerAdapter
        clubJobOfferExperienceSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        // improves user experience depending on which checkbox(es) is/are selected
        manageCheckboxes()

        // updates the club job offers list using the specified criteria
        updateClubJobOfferButton.setOnClickListener {
            if (clubJobOfferDeselectAllCheckboxes.isChecked) {
                Toast.makeText(
                    this,
                    R.string.club_job_offer_no_criteria_chosen_error_text,
                    Toast.LENGTH_LONG
                ).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            } else {
                if (validateParameters()) {
                    // sends the validated request
                    clubJobOfferSearchDTO =
                        JobOfferSearchDTO(CLUB, BLANK, clubName, profession, location, experience)

                    val clubJobOffersAdapter = ClubJoinJobOffersAdapter(clubJobOfferSearchDTO, this)
                    clubJobOffersRecyclerView.layoutManager = LinearLayoutManager(this)
                    clubJobOffersRecyclerView.adapter = clubJobOffersAdapter
                }
            }
        }

    }

    private fun manageCheckboxes() {
        // selects all checkboxes
        clubJobOfferSelectAllCheckboxes.setOnClickListener {
            clubJobOfferNameCheckBox.setChecked(true)
            clubJobOfferLocationCheckBox.setChecked(true)
            clubJobOfferProfessionCheckBox.setChecked(true)
            clubJobOfferExperienceCheckBox.setChecked(true)
            clubJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        // deselects all checkboxes
        clubJobOfferDeselectAllCheckboxes.setOnClickListener {
            clubJobOfferNameCheckBox.setChecked(false)
            clubJobOfferLocationCheckBox.setChecked(false)
            clubJobOfferProfessionCheckBox.setChecked(false)
            clubJobOfferExperienceCheckBox.setChecked(false)
            clubJobOfferSelectAllCheckboxes.setChecked(false)
        }

        // deselects "All checkboxes" since neither none nor all of them are checked
        clubJobOfferNameCheckBox.setOnClickListener {
            clubJobOfferSelectAllCheckboxes.setChecked(false)
            clubJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        clubJobOfferLocationCheckBox.setOnClickListener {
            clubJobOfferSelectAllCheckboxes.setChecked(false)
            clubJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        clubJobOfferProfessionCheckBox.setOnClickListener {
            clubJobOfferSelectAllCheckboxes.setChecked(false)
            clubJobOfferDeselectAllCheckboxes.setChecked(false)
        }

        clubJobOfferExperienceCheckBox.setOnClickListener {
            clubJobOfferSelectAllCheckboxes.setChecked(false)
            clubJobOfferDeselectAllCheckboxes.setChecked(false)
        }
    }

    private fun validateParameters(): Boolean {
        var validCriteria = true

        if (clubJobOfferNameCheckBox.isChecked) {
            if (validateClubJobOfferName()) {
                clubName = clubJobOfferNameText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.club_name_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (clubJobOfferLocationCheckBox.isChecked) {
            if (validateClubJobOfferLocation()) {
                location = clubJobOfferLocationText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.club_job_offer_location_error_text, Toast.LENGTH_LONG)
                    .show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (clubJobOfferProfessionCheckBox.isChecked) {
            profession = clubJobOfferProfessionSpinner.selectedItem.toString()
        }

        if (clubJobOfferExperienceCheckBox.isChecked) {
            experience = clubJobOfferExperienceSpinner.selectedItem.toString()
        }
        return validCriteria
    }

    /**
     * Validates the name input before searching specific clubs which include this criteria
     */
    fun validateClubJobOfferName(): Boolean {
        var validClubName = true

        val clubJobOfferNameTextValue = clubJobOfferNameText!!.text.toString()

        if (clubJobOfferNameTextValue.isEmpty() || clubJobOfferNameTextValue.length < 5) {
            clubJobOfferNameText!!.error = resources.getString(R.string.club_name_error_text)
            validClubName = false
        } else {
            clubJobOfferNameText!!.error = null
        }

        return validClubName
    }

    /**
     * Validates the location input before searching specific clubs which include this criteria
     */
    fun validateClubJobOfferLocation(): Boolean {
        var validClubLocation = true

        val clubJobOfferLocationTextValue = clubJobOfferLocationText!!.text.toString()

        if (clubJobOfferLocationTextValue.isEmpty()) {
            clubJobOfferLocationText!!.error =
                resources.getString(R.string.club_location_error_text)
            validClubLocation = false
        } else {
            clubJobOfferLocationText!!.error = null
        }

        return validClubLocation
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

