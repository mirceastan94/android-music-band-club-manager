package com.manager.client.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_band_job_offer_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the band job offer details activity implementation
 */
class BandJobOfferDetailsActivity : AppCompatActivity() {

    lateinit var senderEmailAddress: String
    lateinit var bandDetails: BandDetailsDTO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_job_offer_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("bandJobOfferDetails") as Bundle
        bandNameText.setText(bundle.get("bandName").toString())
        professionText.setText(bundle.get("profession").toString())
        locationText.setText(bundle.get("location").toString())
        experienceText.setText(bundle.get("experience").toString())
        jobDescriptionText.setText(bundle.get("jobDescription").toString())

        val isOwner: Boolean = extras.get("isOwner") as Boolean
        if (isOwner) {
            bandDetailsButton.setVisibility(View.INVISIBLE)
            bandDetailsButton.setClickable(false)
            applyButton.setVisibility(View.INVISIBLE)
            applyButton.setClickable(false)
        } else {

            retrieveOfferBand(bundle.get("bandName").toString())
            retrieveCurrentUser()

            bandDetailsButton.setOnClickListener {

                val detailsIntent = Intent(this, BandDetailsActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                detailsIntent.putExtra("bandDetails", bandDetails?.toBundle())
                startActivity(detailsIntent)
            }

            applyButton.setOnClickListener {

                val detailsIntent = Intent(applicationContext, SendMailActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                detailsIntent.putExtra("mailSender", senderEmailAddress)
                detailsIntent.putExtra("mailRecipient", bandDetails.ownerEmail)
                startActivity(detailsIntent)
            }
        }

    }

    /**
     * Retrieves the band details
     */
    fun retrieveCurrentUser() {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getLoggedInUser = retrofitInstance?.getLoggedInUserDetails()
        getLoggedInUser?.enqueue(object : Callback<UserDetailsDTO> {
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponse: Response<UserDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    senderEmailAddress = apiResponse.body()!!.email
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at band job offer details", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the band details
     */
    fun retrieveOfferBand(name: String) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getSelectedBandDetails = retrofitInstance?.getSelectedBandDetails(name)
        getSelectedBandDetails?.enqueue(object : Callback<BandDetailsDTO> {
            override fun onResponse(call: Call<BandDetailsDTO>?, apiResponse: Response<BandDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    bandDetails = apiResponse.body()!!
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<BandDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at band job offer details", throwable.toString())
            }
        })

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage);
        super.attachBaseContext(context)
    }

}