package com.manager.client.ui.activities

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.main.SendEmailDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_job_offer_email.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the send mail activity implementation
 */
class SendMailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_offer_email)

        val extras = intent.extras
        val mailSender = extras.get("mailSender") as String
        val mailRecipient = extras.get("mailRecipient") as String
        jobEmailSenderText.setText(mailSender)
        jobEmailRecipientText.setText(mailRecipient)

        inquireEmailButton.setOnClickListener {
            showSendEmailDialog(mailSender, mailRecipient, jobEmailContentText.text.toString())
        }

    }

    /**
     * Displays the send email dialog
     */
    fun showSendEmailDialog(mailSender: String, mailRecipient: String, jobEmailContentText: String) {

        val builder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
        val inflater = layoutInflater
        val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_edit_text, null)
        builder.setMessage(R.string.job_offer_mail_password_text).setCancelable(true).setView(dialogLayout).setPositiveButton(R.string.alert_send_text,
            DialogInterface.OnClickListener { _, _ ->
                val emailPasswordText = dialogLayout.findViewById<EditText>(R.id.emailPasswordText).text.toString()
                val sendEmailDetailsDTO = SendEmailDetailsDTO(mailSender, emailPasswordText, mailRecipient, jobEmailContentText)
                sendEmail(sendEmailDetailsDTO)
            }).setNegativeButton(R.string.alert_cancel_text,
            DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })
        builder.show()
    }

    /**
     * Sends the email to the owner
     */
    fun sendEmail(sendEmailDetailsDTO: SendEmailDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.sendEmail(sendEmailDetailsDTO)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    Toast.makeText(applicationContext, R.string.job_offer_mail_sent_text, Toast.LENGTH_LONG).show()
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                    Toast.makeText(applicationContext, R.string.job_offer_mail_error_text, Toast.LENGTH_LONG).show()
                    MediaPlayerManager.play(applicationContext, R.raw.sound_error)
                }
            }

            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

