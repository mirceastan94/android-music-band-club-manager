package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.EditUserDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.activity_user_edit.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the user edit activity implementation
 */
class UserUpdateActivity : AppCompatActivity() {

    lateinit var genreSpinner: Spinner
    lateinit var professionSpinner: Spinner
    lateinit var currentEditUser: UserDetailsDTO

    var emailDefaultInput: String? = null

    var nameInput: EditText? = null
    var emailInput: EditText? = null
    var passwordInput: EditText? = null
    var phoneInput: EditText? = null
    var locationInput: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_edit)

        // initializes the user input fields
        nameInput = findViewById(R.id.nameText) as EditText
        emailInput = findViewById(R.id.emailText) as EditText
        passwordInput = findViewById(R.id.passwordText) as EditText
        phoneInput = findViewById(R.id.phoneText) as EditText
        locationInput = findViewById(R.id.locationText) as EditText

        // displays the current user details
        retrieveCurrentUser()

        val samplesMap = mapMusicGenresToSongSamples()

        // sets the spinner adapter
        genreSpinner = findViewById(R.id.genreSpinner) as Spinner
        val spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.music_genres, R.layout.spinner_item)
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        var spinnerFirstSelection = true;
        genreSpinner.adapter = spinnerAdapter

        // plays the song sample based on the selected genre
        genreSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (spinnerFirstSelection == false) {
                    val selectedGenre = genreSpinner.selectedItem.toString()
                    MediaPlayerManager.play(getApplicationContext(), samplesMap.get(selectedGenre)!!)
                }
                spinnerFirstSelection = false;
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        // sets the profession spinner adapter
        professionSpinner = findViewById(R.id.professionSpinner) as Spinner
        val professionSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.user_profession, R.layout.spinner_item)
        professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
        professionSpinner.adapter = professionSpinnerAdapter

        // plays the song sample based on the selected genre
        professionSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedProfession = professionSpinner.selectedItem.toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        // handles Edit button press
        editButton.setOnClickListener {
            if (!validateUserInput())
                onEditFailed()
            else {
                editUser()
            }
        }

        // handles Cancel button press
        cancelButton.setOnClickListener {
            finish()
        }

    }

    /**
     * Validates the user data input before beginning the edit process
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val name = nameInput!!.text.toString()
        val email = emailInput!!.text.toString()
        val password = passwordInput!!.text.toString()
        val phone = phoneInput!!.text.toString()
        val location = locationInput!!.text.toString()

        if (name.isEmpty() || name.length < 4) {
            nameInput!!.error = resources.getString(R.string.user_name_error_text)
            validUserData = false
        } else {
            nameInput!!.error = null
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailInput!!.error = resources.getString(R.string.user_email_error_text)
            validUserData = false
        } else {
            emailInput!!.error = null
        }

        if (password.isEmpty() || password.length < 6) {
            passwordInput!!.error = resources.getString(R.string.user_password_error_text)
            validUserData = false
        } else {
            passwordInput!!.error = null
        }

        if (phone.isEmpty() || !android.util.Patterns.PHONE.matcher(phone).matches()) {
            phoneInput!!.error = resources.getString(R.string.user_phone_number_error_text)
            validUserData = false
        } else {
            phoneInput!!.error = null
        }

        if (location.isBlank()) {
            locationInput!!.error = resources.getString(R.string.user_location_error_text)
            validUserData = false
        } else {
            locationInput!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onEditFailed() {
        MediaPlayerManager.play(this, R.raw.sound_error)
    }

    /**
     * Retrieves the current user
     */
    fun retrieveCurrentUser() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getLoggedInUser = retrofitInstance?.getLoggedInUserDetails()

        getLoggedInUser?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponse: Response<UserDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    currentEditUser = apiResponse.body()!!
                    emailDefaultInput = currentEditUser.email
                    nameInput?.setText(currentEditUser.name)
                    emailInput?.setText(currentEditUser.email)
                    passwordInput?.setHint(R.string.user_password_edit_hint_text)
                    phoneInput?.setText(currentEditUser.number)
                    locationInput?.setText(currentEditUser.location)
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at user update", throwable.toString())
            }
        })
    }

    /**
     * Implements the user edit process
     */
    fun editUser() {

        val progressLayout = findViewById(R.id.progressLayout) as ConstraintLayout

        editProgressText.setText(R.string.edit_in_progress_text)
        editLayout.setVisibility(View.INVISIBLE)
        progressLayout.setVisibility(View.VISIBLE)

        val userDTO = EditUserDTO(
            currentEditUser.id,
            nameInput!!.text.toString(),
            emailInput!!.text.toString(),
            passwordInput!!.text.toString(),
            phoneInput!!.text.toString(),
            locationInput!!.text.toString(),
            genreSpinner.selectedItem.toString(),
            professionSpinner.selectedItem.toString()
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val registerRequest = retrofitInstance?.editUser(currentEditUser.id, userDTO)

        registerRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), userDTO.toString())
                    editProgressText.setText(R.string.edit_successful_text)
                    ActivityHelper.startActivity(applicationContext, LoginActivity::class.java)
                    finish()
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string());
                    Log.e("Call response", errorResponse.message)
                    editProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiResponseDTO?.code()) {
                        400 -> editProgressText.setText(R.string.edit_failed_user_exists_text)
                        else -> editProgressText.setText(R.string.edit_failed_internal_error_text)
                    }
                    editLayout.setVisibility(View.VISIBLE)
                    editLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
                signUpLayout.setVisibility(View.VISIBLE)
                editLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}

