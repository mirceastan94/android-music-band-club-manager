package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.user_management.admin.ClubsDashboardAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_dashboard_clubs.*

/**
 * This is the clubs dashboard implementation
 */
class ClubsDashboardActivity : AppCompatActivity() {

    lateinit var clubsDashboardAdapter: ClubsDashboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_clubs)

        clubsDashboardAdapter = ClubsDashboardAdapter()

        clubsRecycleView.layoutManager = LinearLayoutManager(this)
        clubsRecycleView.adapter = clubsDashboardAdapter

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

