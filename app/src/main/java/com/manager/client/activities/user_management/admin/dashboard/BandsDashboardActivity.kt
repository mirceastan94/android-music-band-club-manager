package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.user_management.admin.BandsDashboardAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_dashboard_bands.*

/**
 * This is the bands dashboard activity implementation
 */
class BandsDashboardActivity : AppCompatActivity() {

    lateinit var bandsDashboardAdapter: BandsDashboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_bands)

        bandsDashboardAdapter = BandsDashboardAdapter()

        bandsRecycleView.layoutManager = LinearLayoutManager(this)
        bandsRecycleView.adapter = bandsDashboardAdapter

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

