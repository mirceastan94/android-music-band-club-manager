package com.manager.client.ui.activities

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.manager.client.R
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the main activity implementation
 */
class AccountActivity : AppCompatActivity() {

    lateinit var userDetails: UserDetailsDTO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        var getLoggedInUser = GetLoggedInUser(this)
        userDetails = getLoggedInUser.execute().get()

        var userName = userDetails.name
        if (userName.toLowerCase().equals(MASTER_USER)) {
            deleteAccountImageView.setImageAlpha(75)
        }

        var userRole = userDetails.role
        if (userRole.toLowerCase().contains(USER)) {
            adminDashboardImageView.setImageAlpha(75)
        }
        if (userRole.toLowerCase().contains(USER)) {
            adminDashboardImageView.setImageAlpha(75)
        }

        inboxImageView?.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(GMAIL_URL)
            startActivity(openURL)
        }

        adminDashboardImageView?.setOnClickListener {
            if (userRole.toLowerCase().contains(USER)) {
                MediaPlayerManager.play(this, R.raw.sound_error);
                val alertBuilder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
                alertBuilder.setMessage(R.string.alert_admin_message_text).setCancelable(true)
                    .setPositiveButton(R.string.alert_ok_text,
                        DialogInterface.OnClickListener { dialog, _ ->
                            dialog.cancel()
                        })
                val alert = alertBuilder.create()
                alert.show()
            } else {
                ActivityHelper.startActivity(this, MainDashboardActivity::class.java)
            }
        }

        editAccountImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, UserUpdateActivity::class.java)
        }

        deleteAccountImageView?.setOnClickListener {
            if (userName.toLowerCase().equals(MASTER_USER.toLowerCase())) {
                MediaPlayerManager.play(this, R.raw.sound_error);
                val alertBuilder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
                alertBuilder.setMessage(R.string.alert_super_user_message_text).setCancelable(true)
                    .setPositiveButton(R.string.alert_ok_text,
                        DialogInterface.OnClickListener { dialog, _ ->
                            dialog.cancel()
                        })
                val alert = alertBuilder.create()
                alert.show()
            } else {
                MediaPlayerManager.play(this, R.raw.sound_warning);
                val alertBuilder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
                alertBuilder.setMessage(R.string.alert_account_message_text).setCancelable(true)
                    .setPositiveButton(R.string.alert_yes_text,
                        DialogInterface.OnClickListener { _, _ ->
                            deleteUser()
                        }).setNegativeButton(R.string.alert_no_text,
                        DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

                val alert = alertBuilder.create()
                alert.show()
            }
        }
    }

    /**
     * Implements the user deletion process
     */
    fun deleteUser() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val deleteAccountRequest = retrofitInstance?.removeAccount()

        deleteAccountRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v("Response", apiResponseDTO.toString())
                    ActivityHelper.startActivity(applicationContext, SplashActivity::class.java)
                    finish()
                } else {
                    Log.e("Call response code", apiResponseDTO?.code().toString())
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
                loginLayout.setVisibility(View.VISIBLE)
                loginProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

    /**
     * Handles the synchronous call to determine the user role for the Admin dashboard permission/interdiction
     */
    companion object {
        class GetLoggedInUser internal constructor(context: AccountActivity) : AsyncTask<Void, Void, UserDetailsDTO>() {

            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun doInBackground(vararg params: Void?): UserDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getLoggedInUser = retrofitInstance?.getLoggedInUserDetails()
                var response = getLoggedInUser?.execute()
                var userRole = response?.body()!!
                return userRole
            }

            override fun onPostExecute(result: UserDetailsDTO?) {
                super.onPostExecute(result)
            }
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

