package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.core_functions.events.UserFutureEventsAdapter
import com.manager.client.adapters.core_functions.events.UserPastEventsAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_events_list.*

/**
 * This is the user events list activity implementation
 */
class EventsListActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events_list)

        val userPastEventsAdapter = UserPastEventsAdapter(this)
        pastEventsRecyclerView.layoutManager = LinearLayoutManager(this)
        pastEventsRecyclerView.adapter = userPastEventsAdapter

        val userFutureEventsAdapter = UserFutureEventsAdapter(this)
        futureEventsRecyclerView.layoutManager = LinearLayoutManager(this)
        futureEventsRecyclerView.adapter = userFutureEventsAdapter

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}