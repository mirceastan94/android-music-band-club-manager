package com.manager.client.ui.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.events.dto.EventDetailsDTO
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_notification_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the notifications activity implementation
 */
class NotificationDetailsActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences
    lateinit var chosenLanguage: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("notificationDetails") as Bundle
        val subjectType =
            bundle.get("subjectType").toString().toLowerCase().replace(UNDERLINE, SPACE)

        notificationSubjectTypeText.setText(subjectType.substring(0, 1).toUpperCase() + subjectType.substring(1))

        if (subjectType.toLowerCase().contains(USER)) {
            notificationInquireButton.setVisibility(View.INVISIBLE)
            notificationInquireButton.setEnabled(false)
        }

        when (chosenLanguage) {
            EN -> notificationContentText.setText(bundle.get("textEnglish").toString())
            DE -> notificationContentText.setText(bundle.get("textGerman").toString())
        }

        notificationInquireButton.setOnClickListener {
            when (subjectType) {
                BAND -> getBandDetails(bundle.get("entityId") as Long)
                BAND_JOB_OFFER -> getBandJobOfferDetails(bundle.get("entityId") as Long)
                CLUB -> getClubDetails(bundle.get("entityId") as Long)
                CLUB_JOB_OFFER -> getClubJobOfferDetails(bundle.get("entityId") as Long)
                EVENT -> getEventDetails(bundle.get("entityId") as Long)
            }
        }
    }

    /**
     * Retrieves the selected band details from the database
     */
    fun getBandDetails(id: Long) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getSelectedBandDetails = retrofitInstance?.getSelectedBandDetails(id)
        getSelectedBandDetails?.enqueue(object : Callback<BandDetailsDTO> {
            override fun onResponse(
                call: Call<BandDetailsDTO>?,
                apiResponse: Response<BandDetailsDTO>?
            ) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    val bandDetailsDTO: BandDetailsDTO? = apiResponse.body()
                    val detailsIntent = Intent(applicationContext, BandDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("bandDetails", bandDetailsDTO?.toBundle())
                    applicationContext.startActivity(detailsIntent)
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<BandDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at notification details", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected band job offer details from the database
     */
    fun getBandJobOfferDetails(id: Long) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getBandJobOfferDetails = retrofitInstance?.getSelectedJobOfferDetails(id)
        getBandJobOfferDetails?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<JobOfferDetailsDTO>?,
                apiResponseDTO: Response<JobOfferDetailsDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent =
                        Intent(applicationContext, BandJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", false)
                    detailsIntent.putExtra(
                        "bandJobOfferDetails",
                        bandJobOfferDetailsDTO?.toBundle()
                    )
                    applicationContext.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected club details from the database
     */
    fun getClubDetails(id: Long) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getSelectedClubDetails = retrofitInstance?.getSelectedClubDetails(id)
        getSelectedClubDetails?.enqueue(object : Callback<ClubDetailsDTO> {
            override fun onResponse(
                call: Call<ClubDetailsDTO>?,
                apiResponse: Response<ClubDetailsDTO>?
            ) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    val clubDetailsDTO: ClubDetailsDTO? = apiResponse.body()
                    val detailsIntent = Intent(applicationContext, ClubDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("clubDetails", clubDetailsDTO?.toBundle())
                    applicationContext.startActivity(detailsIntent)
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<ClubDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at notification details", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected club job offer details from the database
     */
    fun getClubJobOfferDetails(id: Long) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getClubJobOfferDetails = retrofitInstance?.getSelectedJobOfferDetails(id)
        getClubJobOfferDetails?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<JobOfferDetailsDTO>?,
                apiResponseDTO: Response<JobOfferDetailsDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent =
                        Intent(applicationContext, ClubJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", false)
                    detailsIntent.putExtra(
                        "clubJobOfferDetails",
                        clubJobOfferDetailsDTO?.toBundle()
                    )
                    applicationContext.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected event details from the database
     */
    fun getEventDetails(id: Long) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getClubEventDetails = retrofitInstance?.getSelectedEventDetails(id)
        getClubEventDetails?.enqueue(object : Callback<EventDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<EventDetailsDTO>?,
                apiResponseDTO: Response<EventDetailsDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventDetails: EventDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent =
                        Intent(applicationContext, EventsDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOver", false)
                    detailsIntent.putExtra("eventDetails", eventDetails?.toBundle())
                    startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

