package com.manager.client.ui.activities

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.main.ResourceNotFoundException
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_band_menu.*

/**
 * This is the band menu activity implementation
 */
class BandMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_menu)

        var getBandDetails = GetBandDetails(this)
        var bandDetailsDTO: BandDetailsDTO? = getBandDetails.execute().get()

        if (null == bandDetailsDTO) {
            statusBandImageView.setImageAlpha(75)
            statusBandImageView.setEnabled(false)
        } else {
            joinBandImageView.setImageAlpha(75)
            joinBandImageView.setEnabled(false)
        }

        if (bandDetailsDTO != null && !bandDetailsDTO.isCurrentUserOwner) {
            manageBandImageView.setImageAlpha(75)
            manageBandImageView.setEnabled(false)
        }

        manageBandImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, BandManagementActivity::class.java)
        }

        joinBandImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, BandJoinActivity::class.java)
        }

        statusBandImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, BandStatusActivity::class.java)
        }
    }

    /**
     * Handles the synchronous call to determine the band details of the logged in user
     */
    companion object {
        class GetBandDetails internal constructor(context: BandMenuActivity) : AsyncTask<Void, Void, BandDetailsDTO>() {

            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getBand = retrofitInstance?.getBand()
                var response = getBand?.execute()
                val bandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    bandDetailsDTO = response.body()!!
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(ResourceNotFoundException::class.java)
                    val errorResponse = adapter.fromJson(response?.errorBody()?.string())
                    Log.e("Call response", errorResponse.resourceName)
                    bandDetailsDTO = null
                }
                return bandDetailsDTO
            }

            override fun onPostExecute(result: BandDetailsDTO?) {
                super.onPostExecute(result)
            }
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

