package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.manager.client.R
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE

/**
 * This is the main activity implementation
 */
class MainDashboardActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_main)

        val usersDashboardImgView = findViewById(R.id.usersDashboardImageView) as? ImageView
        val bandsDashboardImgView = findViewById(R.id.bandsDashboardImageView) as? ImageView
        val clubsDashboardImgView = findViewById(R.id.clubsDashboardImageView) as? ImageView
        val eventsDashboardImgView = findViewById(R.id.eventsDashboardImageView) as? ImageView

        usersDashboardImgView?.setOnClickListener {
            ActivityHelper.startActivity(this, UsersDashboardActivity::class.java)
        }
        bandsDashboardImgView?.setOnClickListener {
            ActivityHelper.startActivity(this, BandsDashboardActivity::class.java)
        }
        clubsDashboardImgView?.setOnClickListener {
            ActivityHelper.startActivity(this, ClubsDashboardActivity::class.java)
        }
        eventsDashboardImgView?.setOnClickListener {
            ActivityHelper.startActivity(this, EventsDashboardActivity::class.java)
        }

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

