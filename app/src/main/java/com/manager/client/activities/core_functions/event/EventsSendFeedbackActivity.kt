package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.CreateReviewDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_event_send_feedback.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the event send feedback activity implementation
 */
class EventsSendFeedbackActivity : AppCompatActivity() {

    var userId: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_send_feedback)

        val extras = intent.extras
        val bundle: Bundle = extras.get("eventDetails") as Bundle
        retrieveCurrentUser()

        eventSendFeedbackButton.setOnClickListener {
            if (!validateUserInput())
                onSignupFailed()
            else {
                sendFeedback(bundle)
            }
        }
    }

    /**
     * Validates the user data input before beginning the event send feedback process
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val clubRatingValue = eventClubRatingBar.rating
        val bandRatingValue = eventBandRatingBar.rating
        val eventSendFeedbackTextValue = eventSendFeedbackText!!.text.toString()

        if (eventSendFeedbackTextValue.isEmpty() || eventSendFeedbackTextValue.length < 6) {
            eventSendFeedbackText!!.error = resources.getString(R.string.event_send_feedback_too_short_error_text)
            validUserData = false
        } else {
            eventSendFeedbackText!!.error = null
        }

        if (clubRatingValue == 0.0.toFloat()) {
            Toast.makeText(this, R.string.event_send_feedback_no_club_rating_error_text, Toast.LENGTH_LONG).show()


        }
        if (bandRatingValue == 0.0.toFloat()) {
            Toast.makeText(this, R.string.event_send_feedback_no_band_rating_error_text, Toast.LENGTH_LONG).show()
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onSignupFailed() {
        MediaPlayerManager.play(this, R.raw.sound_error)
    }

    /**
     * Implements the event send feedback
     */
    fun sendFeedback(bundle: Bundle) {

        eventSendFeedbackProgressText.setText(R.string.event_send_feedback_in_progress_text)
        eventSendFeedbackLayout.setVisibility(View.INVISIBLE)
        eventSendFeedbackInProgressLayout.setVisibility(View.VISIBLE)

        val reviewDTO = CreateReviewDTO(
            eventSendFeedbackText!!.text.toString(),
            eventBandRatingBar.rating.toDouble(),
            eventClubRatingBar.rating.toDouble(),
            bundle.get("ID").toString().toLong(),
            userId
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val sendFeedbackRequest = retrofitInstance?.createReview(reviewDTO)

        sendFeedbackRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), reviewDTO.toString())
                    eventSendFeedbackProgressText.setText(R.string.club_create_successful_text)
                    eventSendFeedbackProgressBar.setVisibility(View.INVISIBLE)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
                eventSendFeedbackLayout.setVisibility(View.VISIBLE)
                eventSendFeedbackInProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

    /**
     * Retrieves the current user
     */
    fun retrieveCurrentUser() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getLoggedInUser = retrofitInstance?.getLoggedInUserDetails()

        getLoggedInUser?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponse: Response<UserDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    userId = apiResponse.body()!!.id
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at user update", throwable.toString())
            }
        })
    }


    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

