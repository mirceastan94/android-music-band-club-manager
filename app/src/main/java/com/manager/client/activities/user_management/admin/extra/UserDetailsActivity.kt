package com.manager.client.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.manager.client.R
import kotlinx.android.synthetic.main.activity_user_details.*


class UserDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("UserDetails") as Bundle
        nameText.setText(bundle.get("Name").toString())
        emailText.setText(bundle.get("Email").toString())
        phoneText.setText(bundle.get("Number").toString())
        locationText.setText(bundle.get("Location").toString())
        musicGenreText.setText(bundle.get("Genre").toString())
        professionText.setText(bundle.get("Profession").toString())

    }
}