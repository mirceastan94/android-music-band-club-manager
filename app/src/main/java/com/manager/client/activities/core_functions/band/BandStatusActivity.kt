package com.manager.client.ui.activities

import android.content.Context
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.general.BandOverviewMembersAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_band_status.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the band status activity implementation
 */
class BandStatusActivity : AppCompatActivity() {

    lateinit var bandOverviewMembersAdapter: BandOverviewMembersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_status)

        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        statusBandNameText.setText(userBandDetails.name)
        statusBandLocationText.setText(userBandDetails.location)
        statusBandMusicGenreText.setText(userBandDetails.genre)
        statusBandRatingBar.setRating(userBandDetails.rating.toFloat())

        bandOverviewMembersAdapter = BandOverviewMembersAdapter()

        statusBandMembersRecyclerView.layoutManager = LinearLayoutManager(this)
        statusBandMembersRecyclerView.adapter = bandOverviewMembersAdapter

        resignButton?.setOnClickListener {
            showResignFromBandDialog()
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

    /**
     * Displays the member resign dialog
     */
    fun showResignFromBandDialog() {
        MediaPlayerManager.play(this, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(this, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.band_status_resign_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    resignFromBand()
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

    /**
     * Deletes the member from the band
     */
    fun resignFromBand() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.resignFromBand()
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    ActivityHelper.startActivity(applicationContext, MainActivity::class.java)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandStatusActivity) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }
}

