package com.manager.client.ui.activities

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.manager.client.R
import com.manager.client.adapters.core_functions.events.EventDetailsAdapter
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_event_details.*
import java.time.Duration
import java.time.LocalDateTime


/**
 * This is the event details activity implementation
 */
class EventsDetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("eventDetails") as Bundle

        // populates the main details
        clubNameText.setText(bundle.get("clubName").toString())
        bandNameText.setText(bundle.get("bandName").toString())
        locationText.setText(bundle.get("location").toString())
        dateTimeText.setText(bundle.get("dateTime").toString().replace(T, COMMA))

        // shows the participants
        var eventDetailsAdapter = EventDetailsAdapter(bundle.get("ID").toString())

        eventParticipantsRecyclerView.layoutManager = LinearLayoutManager(this)
        eventParticipantsRecyclerView.adapter = eventDetailsAdapter

        // shows the correct bottom layout depending on the event timeline
        val isOver: Boolean = extras.get("isOver") as Boolean
        if (isOver) {
            eventFeedbackLayout.setVisibility(View.VISIBLE)
            eventFeedbackLayout.setClickable(true)
            eventRemainingTimeLayout.setVisibility(View.INVISIBLE)
            eventRemainingTimeLayout.setClickable(false)
        } else {
            val eventDateTime = LocalDateTime.parse(bundle.get("dateTime").toString())
            val currentDateTime = LocalDateTime.now()
            remainingTimeText.setText(getString(R.string.event_remaining_time_text).plus(SPACE).plus(Duration.between(currentDateTime, eventDateTime).toHours().toString() + h))
        }

        // calls Google Maps module with the club location set
        mapImageView.setOnClickListener {
            val navigationIntentUri = Uri.parse("google.navigation:q=".plus(bundle.get("location").toString()))
            val mapIntent = Intent(Intent.ACTION_VIEW, navigationIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }

        // launches the event feedback list activity
        eventFeedbackListText.setOnClickListener {
            val detailsIntent = Intent(applicationContext, EventsFeedbackListActivity::class.java)
            detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            detailsIntent.putExtra("eventDetails", bundle)
            startActivity(detailsIntent)
        }

        // launches the send feedback activity
        eventSendFeedbackText.setOnClickListener {
            val detailsIntent = Intent(applicationContext, EventsSendFeedbackActivity::class.java)
            detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            detailsIntent.putExtra("eventDetails", bundle)
            startActivity(detailsIntent)
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}