package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.events.EventsJoinAdapter
import com.manager.client.models.events.dto.EventSearchDTO
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.activity_events_join.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * This is the events join activity implementation
 */
class EventsJoinActivity : AppCompatActivity() {

    var clubName: String = ""
    var bandName: String = ""
    var location: String = ""
    var fromLocalDate: LocalDate? = LocalDate.now()
    var toLocalDate: LocalDate? = LocalDate.now().plusDays(7)
    var eventSearchDTO: EventSearchDTO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events_join)

        val eventsJoinAdapter = EventsJoinAdapter(eventSearchDTO, this)

        availableEventsRecyclerView.layoutManager = LinearLayoutManager(this)
        availableEventsRecyclerView.adapter = eventsJoinAdapter

        // improves user experience depending on which checkbox(es) is/are selected
        manageCheckboxes()

        // sets the events beginning interval
        setStartingDateInterval()

        // sets the events ending interval
        setEndingDateInterval()

        // updates the events list using the specified criteria
        updateEventsListButton.setOnClickListener {
            if (eventsDeselectAllCheckboxes.isChecked) {
                Toast.makeText(this, R.string.club_job_offer_no_criteria_chosen_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            } else {
                if (validateParameters()) {
                    // sends the validated request
                    if (eventsIntervalCheckBox.isChecked) {
                        eventSearchDTO = EventSearchDTO(clubName, bandName, location, fromLocalDate.toString(), toLocalDate.toString())
                    } else {
                        eventSearchDTO = EventSearchDTO(clubName, bandName, location, "", "")
                    }
                    val eventsJoinAdapter = EventsJoinAdapter(eventSearchDTO, this)
                    availableEventsRecyclerView.layoutManager = LinearLayoutManager(this)
                    availableEventsRecyclerView.adapter = eventsJoinAdapter
                }
            }
        }
    }

    private fun setStartingDateInterval() {
        eventsFromDateButton?.setOnClickListener {
            eventsFromCalendarView.setMinDate(System.currentTimeMillis() - 1000)
            eventsFromCalendarLayout.setVisibility(View.VISIBLE)
            eventsInputLayout.setVisibility(View.INVISIBLE)
        }

        eventsFromCalendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            var dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE
            var formattedMonth: String
            var formattedDay: String
            if (month >= 9) {
                formattedMonth = (month + 1).toString()
            } else {
                formattedMonth = ZERO.plus(month + 1)
            }
            if (dayOfMonth < 10) {
                formattedDay = ZERO.plus(dayOfMonth)
            } else {
                formattedDay = dayOfMonth.toString()
            }
            fromLocalDate = LocalDate.parse(year.toString().plus(LINE).plus(formattedMonth).plus(LINE).plus(formattedDay), dateTimeFormatter)
        }

        eventsFromChooseDateButton?.setOnClickListener {
            eventsFromCalendarLayout.setVisibility(View.INVISIBLE)
            eventsInputLayout.setVisibility(View.VISIBLE)
        }
    }

    private fun setEndingDateInterval() {
        eventsToDateButton?.setOnClickListener {
            eventsToCalendarView.setMinDate(System.currentTimeMillis() - 1000)
            eventsToCalendarLayout.setVisibility(View.VISIBLE)
            eventsInputLayout.setVisibility(View.INVISIBLE)
        }

        eventsToCalendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
            var dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE
            var formattedMonth: String
            var formattedDay: String
            if (month >= 9) {
                formattedMonth = (month + 1).toString()
            } else {
                formattedMonth = ZERO.plus(month + 1)
            }
            if (dayOfMonth < 10) {
                formattedDay = ZERO.plus(dayOfMonth)
            } else {
                formattedDay = dayOfMonth.toString()
            }
            fromLocalDate = LocalDate.parse(year.toString().plus(LINE).plus(formattedMonth).plus(LINE).plus(formattedDay), dateTimeFormatter)
        }

        eventsToChooseDateButton?.setOnClickListener {
            eventsToCalendarLayout.setVisibility(View.INVISIBLE)
            eventsInputLayout.setVisibility(View.VISIBLE)
        }
    }

    private fun manageCheckboxes() {
        // selects all checkboxes
        eventsSelectAllCheckboxes.setOnClickListener {
            eventsClubNameCheckBox.setChecked(true)
            eventsBandNameCheckBox.setChecked(true)
            eventsLocationCheckBox.setChecked(true)
            eventsIntervalCheckBox.setChecked(true)
            eventsDeselectAllCheckboxes.setChecked(false)
        }

        // deselects all checkboxes
        eventsDeselectAllCheckboxes.setOnClickListener {
            eventsClubNameCheckBox.setChecked(false)
            eventsBandNameCheckBox.setChecked(false)
            eventsLocationCheckBox.setChecked(false)
            eventsIntervalCheckBox.setChecked(false)
            eventsSelectAllCheckboxes.setChecked(false)
        }

        // deselects "All checkboxes" since neither none nor all of them are checked
        eventsClubNameCheckBox.setOnClickListener {
            eventsSelectAllCheckboxes.setChecked(false)
            eventsDeselectAllCheckboxes.setChecked(false)
        }

        eventsBandNameCheckBox.setOnClickListener {
            eventsSelectAllCheckboxes.setChecked(false)
            eventsDeselectAllCheckboxes.setChecked(false)
        }

        eventsLocationCheckBox.setOnClickListener {
            eventsSelectAllCheckboxes.setChecked(false)
            eventsDeselectAllCheckboxes.setChecked(false)
        }

        eventsIntervalCheckBox.setOnClickListener {
            eventsSelectAllCheckboxes.setChecked(false)
            eventsDeselectAllCheckboxes.setChecked(false)
        }
    }

    private fun validateParameters(): Boolean {
        var validCriteria = true

        if (eventsClubNameCheckBox.isChecked) {
            if (validateEventsClubName()) {
                clubName = eventsClubNameText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.club_name_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (eventsBandNameCheckBox.isChecked) {
            if (validateEventsBandName()) {
                bandName = eventsBandNameText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.club_name_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        if (eventsLocationCheckBox.isChecked) {
            if (validateEventsLocation()) {
                location = eventsLocationText!!.text.toString()
            } else {
                validCriteria = false
                Toast.makeText(this, R.string.club_job_offer_location_error_text, Toast.LENGTH_LONG).show()
                MediaPlayerManager.play(this, R.raw.sound_warning)
            }
        }

        return validCriteria
    }

    /**
     * Validates the club name input before searching specific events which include this criteria
     */
    fun validateEventsClubName(): Boolean {
        var validClubName = true

        val eventsClubNameTextValue = eventsClubNameText!!.text.toString()

        if (eventsClubNameTextValue.isEmpty() || eventsClubNameTextValue.length < 5) {
            eventsClubNameText!!.error = resources.getString(R.string.club_name_error_text)
            validClubName = false
        } else {
            eventsClubNameText!!.error = null
        }

        return validClubName
    }

    /**
     * Validates the band name input before searching specific events which include this criteria
     */
    fun validateEventsBandName(): Boolean {
        var validBandName = true

        val eventsBandNameTextValue = eventsBandNameText!!.text.toString()

        if (eventsBandNameTextValue.isEmpty() || eventsBandNameTextValue.length < 5) {
            eventsBandNameText!!.error = resources.getString(R.string.band_name_error_text)
            validBandName = false
        } else {
            eventsBandNameText!!.error = null
        }

        return validBandName
    }

    /**
     * Validates the club location input before searching specific events which include this criteria
     */
    fun validateEventsLocation(): Boolean {
        var validLocation = true

        val eventsLocationTextValue = eventsLocationText!!.text.toString()

        if (eventsLocationTextValue.isEmpty()) {
            eventsLocationText!!.error = resources.getString(R.string.club_location_error_text)
            validLocation = false
        } else {
            eventsLocationText!!.error = null
        }

        return validLocation
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

