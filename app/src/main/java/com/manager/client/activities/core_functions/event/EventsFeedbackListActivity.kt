package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.manager.client.R
import com.manager.client.adapters.core_functions.events.EventFeedbackListAdapter
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_event_feedback_list.*

/**
 * This is the event feedback list activity implementation
 */
class EventsFeedbackListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_feedback_list)

        val extras = intent.extras
        val bundle: Bundle = extras.get("eventDetails") as Bundle

        // populates the main details
        var mainActivityNotificationsAdapter = EventFeedbackListAdapter(bundle.get("ID").toString())

        eventFeedbackListRecyclerView.layoutManager = LinearLayoutManager(this)
        eventFeedbackListRecyclerView.adapter = mainActivityNotificationsAdapter

    }


    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

