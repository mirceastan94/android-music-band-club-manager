package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import com.manager.client.R
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_band_details.*

/**
 * This is the band details activity implementation
 */
class BandDetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_band_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("bandDetails") as Bundle
        nameText.setText(bundle.get("name").toString())
        ownerText.setText(bundle.get("owner").toString())
        locationText.setText(bundle.get("location").toString())
        musicGenreText.setText(bundle.get("genre").toString())

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}