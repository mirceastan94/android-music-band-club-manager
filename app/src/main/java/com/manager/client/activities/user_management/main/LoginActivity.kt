package com.manager.client.ui.activities

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import com.manager.client.R
import com.manager.client.models.users.dto.JWTAuthResponseDTO
import com.manager.client.models.users.dto.LoginUserDTO
import com.manager.client.network.ManagerAPI
import com.manager.client.network.UnauthenticatedRetrofit
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.*
import com.manager.client.utils.AppContext.Companion.context
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the login activity implementation
 * @author Mircea Stan
 */
class LoginActivity : AppCompatActivity() {

    var emailInput: EditText? = null
    var passwordInput: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        MediaPlayerManager.play(this, R.raw.sound_login)

        emailInput = findViewById(R.id.emailText) as EditText
        passwordInput = findViewById(R.id.passwordText) as EditText

        loginText.setOnClickListener {
            if (!validateUserInput())
                onSignupFailed()
            else {
                loginUser()
            }
        }
        signUpText.setOnClickListener {
            ActivityHelper.startActivity(this, RegisterActivity::class.java)
            finish()
        }

    }

    /**
     * Validates the user data input before beginning the login process
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val email = emailInput!!.text.toString()
        val password = passwordInput!!.text.toString()

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailInput!!.error = resources.getString(R.string.user_email_error_text)
            validUserData = false
        } else {
            emailInput!!.error = null
        }

        if (password.isEmpty() || password.length < 6) {
            passwordInput!!.error = resources.getString(R.string.user_password_error_text)
            validUserData = false
        } else {
            passwordInput!!.error = null
        }
        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onSignupFailed() {
        MediaPlayerManager.play(this, R.raw.sound_error)
    }

    /**
     * Implements the user login process
     */
    fun loginUser() {

        loginProgressText.setText(R.string.login_in_progress_text)
        loginLayout.setVisibility(View.INVISIBLE)
        loginProgressLayout.setVisibility(View.VISIBLE)

        val newLoginRequest = LoginUserDTO(
            emailInput!!.text.toString(),
            passwordInput!!.text.toString()
        )

        val retrofitInstance = UnauthenticatedRetrofit.let { makeRetrofit() }?.create(ManagerAPI::class.java)
        val loginRequest = retrofitInstance?.logIn(newLoginRequest)

        loginRequest?.enqueue(object : Callback<JWTAuthResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<JWTAuthResponseDTO>?,
                apiAuthResponseDTO: Response<JWTAuthResponseDTO>?
            ) {
                if (apiAuthResponseDTO != null && apiAuthResponseDTO.isSuccessful) {
                    Log.v(apiAuthResponseDTO.toString(), newLoginRequest.toString())
                    loginProgressText.setText(R.string.login_successful_text)

                    // saves the JWT in the SharedPreferences for future REST calls
                    val jwt: String = apiAuthResponseDTO.body()!!.accessToken
                    val preferences = PreferenceManager.getDefaultSharedPreferences(context);
                    val editor = preferences.edit()
                    editor.putString(JSON_WEB_TOKEN, jwt).commit()
                    ActivityHelper.startActivity(applicationContext, MainActivity::class.java)
                    finish()
                } else {
                    Log.e("Call response code", apiAuthResponseDTO?.code().toString())
                    loginProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiAuthResponseDTO?.code()) {
                        400 -> loginProgressText.setText(R.string.login_failed_user_internal_error_text)
                        401 -> loginProgressText.setText(R.string.login_failed_invalid_credentials_error_text)
                        else -> loginProgressText.setText(R.string.login_failed_user_internal_error_text)
                    }
                    loginLayout.setVisibility(View.VISIBLE)
                    loginProgressLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JWTAuthResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught at login", throwable.toString())
                loginLayout.setVisibility(View.VISIBLE)
                loginProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}

