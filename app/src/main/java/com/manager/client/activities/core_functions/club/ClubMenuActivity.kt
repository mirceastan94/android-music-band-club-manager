package com.manager.client.ui.activities

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.main.ResourceNotFoundException
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_club_menu.*

/**
 * This is the club menu activity implementation
 */
class ClubMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_menu)

        var getClubDetails = GetClubDetails(this)
        var clubDetailsDTO: ClubDetailsDTO? = getClubDetails.execute().get()

        if (null == clubDetailsDTO) {
            statusClubImageView.setImageAlpha(75)
            statusClubImageView.setEnabled(false)
        } else {
            joinClubImageView.setImageAlpha(75)
            joinClubImageView.setEnabled(false)
        }

        if (clubDetailsDTO != null && !clubDetailsDTO.isCurrentUserOwner) {
            manageClubImageView.setImageAlpha(75)
            manageClubImageView.setEnabled(false)
        }

        manageClubImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, ClubManagementActivity::class.java)
        }

        joinClubImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, ClubJoinActivity::class.java)
        }

        statusClubImageView?.setOnClickListener {
            ActivityHelper.startActivity(this, ClubStatusActivity::class.java)
        }
    }

    /**
     * Handles the synchronous call to determine the club details of the logged in user
     */
    companion object {
        class GetClubDetails internal constructor(context: ClubMenuActivity) : AsyncTask<Void, Void, ClubDetailsDTO>() {

            override fun onPreExecute() {
                super.onPreExecute()
            }

            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getClub = retrofitInstance?.getClub()
                var response = getClub?.execute()
                val clubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    clubDetailsDTO = response.body()!!
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(ResourceNotFoundException::class.java)
                    val errorResponse = adapter.fromJson(response?.errorBody()?.string())
                    Log.e("Call response", errorResponse.resourceName)
                    clubDetailsDTO = null
                }
                return clubDetailsDTO
            }

            override fun onPostExecute(result: ClubDetailsDTO?) {
                super.onPostExecute(result)
            }
        }
    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }
}

