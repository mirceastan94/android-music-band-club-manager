package com.manager.client.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.LocaleHelper
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.activity_club_job_offer_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * This is the club job offer details activity implementation
 */
class ClubJobOfferDetailsActivity : AppCompatActivity() {

    lateinit var senderEmailAddress: String
    lateinit var clubDetails: ClubDetailsDTO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_club_job_offer_details)

        val extras = intent.extras
        val bundle: Bundle = extras.get("clubJobOfferDetails") as Bundle
        clubNameText.setText(bundle.get("clubName").toString())
        professionText.setText(bundle.get("profession").toString())
        locationText.setText(bundle.get("location").toString())
        experienceText.setText(bundle.get("experience").toString())
        jobDescriptionText.setText(bundle.get("jobDescription").toString())

        val isOwner: Boolean = extras.get("isOwner") as Boolean
        if (isOwner) {
            clubDetailsButton.setVisibility(View.INVISIBLE)
            clubDetailsButton.setClickable(false)
            applyButton.setVisibility(View.INVISIBLE)
            applyButton.setClickable(false)
        } else {

            retrieveOfferBand(bundle.get("clubName").toString())
            retrieveCurrentUser()

            clubDetailsButton.setOnClickListener {

                val detailsIntent = Intent(this, ClubDetailsActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                detailsIntent.putExtra("clubDetails", clubDetails?.toBundle())
                startActivity(detailsIntent)
            }

            applyButton.setOnClickListener {

                val detailsIntent = Intent(applicationContext, SendMailActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                detailsIntent.putExtra("mailSender", senderEmailAddress)
                detailsIntent.putExtra("mailRecipient", clubDetails.ownerEmail)
                startActivity(detailsIntent)
            }
        }

    }

    /**
     * Retrieves the club details
     */
    fun retrieveCurrentUser() {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getLoggedInUser = retrofitInstance?.getLoggedInUserDetails()
        getLoggedInUser?.enqueue(object : Callback<UserDetailsDTO> {
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponse: Response<UserDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    senderEmailAddress = apiResponse.body()!!.email
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at club job offer details", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the club details
     */
    fun retrieveOfferBand(name: String) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getSelectedBandDetails = retrofitInstance?.getSelectedClubDetails(name)
        getSelectedBandDetails?.enqueue(object : Callback<ClubDetailsDTO> {
            override fun onResponse(call: Call<ClubDetailsDTO>?, apiResponse: Response<ClubDetailsDTO>?) {
                if (apiResponse != null && apiResponse.isSuccessful) {
                    Log.v("Response code", apiResponse.code().toString())
                    clubDetails = apiResponse.body()!!
                } else {
                    Log.e("Response code", apiResponse?.code().toString())
                }
            }

            override fun onFailure(call: Call<ClubDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught at job offer details activity", throwable.toString())
            }
        })

    }

    /**
     * Sets the locale based on the chosen language
     */
    override fun attachBaseContext(currentContext: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(currentContext)
        val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")
        val context = LocaleHelper.setLocale(currentContext, chosenLanguage)
        super.attachBaseContext(context)
    }

}