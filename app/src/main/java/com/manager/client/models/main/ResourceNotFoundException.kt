package com.manager.client.models.main

import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType

data class ResourceNotFoundException(
    val resourceName: String,
    val fieldName: String,
    val fieldValue: JvmType.Object
)