package com.manager.client.models.events.dto

data class CreateReviewDTO(
    val eventFeedback: String,
    val bandRating: Double,
    val clubRating: Double,
    val eventId: Long,
    val userId: Long
)