package com.manager.client.models.events.dto

import android.os.Bundle

data class EventDetailsDTO(
    val id: Long,
    val clubName: String,
    val bandName: String,
    val capacity: Int,
    val location: String,
    val dateTime: String,
    val isOver: Boolean,
    val isSoldOut: Boolean
) {
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putString("clubName", clubName)
        bundle.putString("bandName", bandName)
        bundle.putInt("capacity", capacity)
        bundle.putString("location", location)
        bundle.putString("dateTime", dateTime)
        bundle.putBoolean("isOver", isOver)
        bundle.putBoolean("isSoldOut", isSoldOut)

        return bundle
    }
}


