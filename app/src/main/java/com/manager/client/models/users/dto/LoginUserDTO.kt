package com.manager.client.models.users.dto

data class LoginUserDTO(
    val email: String,
    val password: String
)
