package com.manager.client.models.club.entities

data class ClubMemberSummary(
    val id: Long,
    val name: String,
    val profession: String,
    val location: String
)
