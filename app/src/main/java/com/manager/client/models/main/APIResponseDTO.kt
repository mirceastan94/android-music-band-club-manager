package com.manager.client.models.main

data class APIResponseDTO(
    val success: Boolean,
    val message: String
)