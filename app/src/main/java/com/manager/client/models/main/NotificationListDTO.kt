package com.manager.client.models.main

data class NotificationListDTO(
    val notificationList: List<NotificationDTO>
)