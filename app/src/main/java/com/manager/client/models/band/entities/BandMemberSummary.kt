package com.manager.client.models.band.entities

data class BandMemberSummary(
    val id: Long,
    val name: String,
    val profession: String,
    val location: String
)
