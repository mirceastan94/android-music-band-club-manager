package com.manager.client.models.club.dto

data class UpdateClubDTO(
    val id: Long,
    val name: String,
    val location: String,
    val genre: String
)
