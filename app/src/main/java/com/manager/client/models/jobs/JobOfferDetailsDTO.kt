package com.manager.client.models.jobs

import android.os.Bundle

data class JobOfferDetailsDTO(
    val id: Long,
    val category: String,
    val bandName: String,
    val clubName: String,
    val profession: String,
    val location: String,
    val experience: String,
    val jobDescription: String
)
{
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putString("category", category)
        bundle.putString("bandName", bandName)
        bundle.putString("clubName", clubName)
        bundle.putString("profession", profession)
        bundle.putString("location", location)
        bundle.putString("experience", experience)
        bundle.putString("jobDescription", jobDescription)

        return bundle;
    }
}
