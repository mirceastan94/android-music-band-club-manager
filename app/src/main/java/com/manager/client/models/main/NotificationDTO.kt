package com.manager.client.models.main

import android.os.Bundle

data class NotificationDTO(
    val id: Long,
    val subjectType: String,
    val textEnglish: String,
    val textGerman: String,
    val entityId: Long
)
{
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putString("subjectType", subjectType)
        bundle.putString("textEnglish", textEnglish)
        bundle.putString("textGerman", textGerman)
        bundle.putLong("entityId", entityId)
        return bundle
    }
}