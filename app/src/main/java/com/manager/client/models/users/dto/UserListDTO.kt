package com.manager.client.models.users.dto

import com.manager.client.models.users.entities.UserSummary

data class UserListDTO(
    val userSummaryList: List<UserSummary>
)