package com.manager.client.models.band.dto

data class UpdateBandDTO(
    val id: Long,
    val name: String,
    val location: String,
    val genre: String
)
