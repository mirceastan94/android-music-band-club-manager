package com.manager.client.models.club.dto

data class CreateClubDTO(
    val name: String,
    val location: String,
    val street: String,
    val genre: String
)
