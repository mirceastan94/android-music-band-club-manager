package com.manager.client.models.events.dto

import android.os.Bundle

data class ReviewDetailsDTO(
    val id: Long,
    val bandRating: Double,
    val clubRating: Double,
    val eventFeedback: String,
    val userName: String,
    val userLocation: String
) {
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putDouble("bandRating", bandRating)
        bundle.putDouble("clubRating", clubRating)
        bundle.putString("eventFeedback", eventFeedback)
        bundle.putString("userName", userName)
        bundle.putString("userLocation", userLocation)

        return bundle
    }
}


