package com.manager.client.models.events.dto

data class ReviewListDTO(
    val reviewSummaryList: List<ReviewDetailsDTO>
)