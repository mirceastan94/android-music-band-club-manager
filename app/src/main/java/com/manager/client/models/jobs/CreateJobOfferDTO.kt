package com.manager.client.models.jobs

data class CreateJobOfferDTO(
    val category: String,
    val profession: String,
    val location: String,
    val experience: String,
    val jobDescription: String,
    val bandName: String?,
    val clubName: String?
)