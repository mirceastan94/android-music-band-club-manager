package com.manager.client.models.band.dto

data class CreateBandDTO(
    val name: String,
    val location: String,
    val genre: String
)
