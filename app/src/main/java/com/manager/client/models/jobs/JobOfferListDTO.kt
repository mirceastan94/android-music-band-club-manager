package com.manager.client.models.jobs

data class JobOfferListDTO(
    val jobOfferDetailsList: List<JobOfferDetailsDTO>
)
