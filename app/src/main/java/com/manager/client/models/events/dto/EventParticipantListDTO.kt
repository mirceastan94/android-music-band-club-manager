package com.manager.client.models.events.dto

import com.manager.client.models.events.entities.EventParticipantSummary

data class EventParticipantListDTO(
    val eventParticipantList: List<EventParticipantSummary>
)