package com.manager.client.models.events.dto

data class EventSearchDTO(
    val clubName: String,
    val bandName: String,
    val location: String,
    val fromDateTime: String,
    val toDateTime: String)
