package com.manager.client.models.band.dto

data class BandListDTO(
    val bandSummaryList: List<BandDetailsDTO>
)