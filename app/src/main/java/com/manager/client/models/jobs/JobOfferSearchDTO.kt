package com.manager.client.models.jobs

data class JobOfferSearchDTO(
    val category: String,
    val bandName: String,
    val clubName: String,
    val profession: String,
    val location: String,
    val experience: String)
