package com.manager.client.models.users.entities

data class UserSummary(
    val id: Long,
    val name: String,
    val profession: String,
    val role: String
)
