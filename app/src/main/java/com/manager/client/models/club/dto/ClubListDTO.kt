package com.manager.client.models.club.dto

data class ClubListDTO(
    val clubSummaryList: List<ClubDetailsDTO>
)