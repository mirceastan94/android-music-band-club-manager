package com.manager.client.models.users.entities

data class MemberSummary(
    val id: Long,
    val name: String,
    val profession: String,
    val location: String
)
