package com.manager.client.models.events.dto

data class EventListDTO(
    val eventSummaryList: List<EventDetailsDTO>
)