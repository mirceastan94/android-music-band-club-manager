package com.manager.client.models.users.dto

data class JWTAuthResponseDTO(
    val accessToken: String,
    val tokenType: String
)