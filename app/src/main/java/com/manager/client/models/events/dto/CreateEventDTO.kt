package com.manager.client.models.events.dto

data class CreateEventDTO(
    val clubName: String,
    val bandName: String,
    val capacity: Int,
    val date: String,
    val time: String,
    val description: String
)
