package com.manager.client.models.club.dto

import com.manager.client.models.club.entities.ClubMemberSummary

data class ClubMemberListDTO(
    val memberSummaryList: List<ClubMemberSummary>
)