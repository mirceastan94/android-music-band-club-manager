package com.manager.client.models.band.dto

import com.manager.client.models.band.entities.BandMemberSummary

data class BandMemberListDTO(
    val memberSummaryList: List<BandMemberSummary>
)