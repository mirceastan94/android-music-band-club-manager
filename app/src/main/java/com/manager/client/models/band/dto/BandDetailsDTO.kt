package com.manager.client.models.band.dto

import android.os.Bundle

data class BandDetailsDTO(
    val id: Long,
    val name: String,
    val location: String,
    val genre: String,
    val rating: Double,
    val ownerName: String,
    val ownerEmail: String,
    val isCurrentUserOwner: Boolean
) {
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putString("name", name)
        bundle.putString("owner", ownerName)
        bundle.putDouble("rating", rating)
        bundle.putString("location", location)
        bundle.putString("genre", genre)

        return bundle;
    }
}


