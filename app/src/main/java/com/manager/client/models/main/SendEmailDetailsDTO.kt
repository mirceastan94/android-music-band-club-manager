package com.manager.client.models.main

data class SendEmailDetailsDTO(
    val senderAddress: String,
    val senderPassword: String,
    val recipientAddress: String,
    val content: String
)