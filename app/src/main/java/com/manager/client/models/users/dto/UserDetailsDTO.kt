package com.manager.client.models.users.dto

import android.os.Bundle

data class UserDetailsDTO(
    val id: Long,
    val name: String,
    val email: String,
    val password: String,
    val number: String,
    val location: String,
    val genre: String,
    val role: String,
    val profession: String
) {
    public fun toBundle(): Bundle {
        var bundle = Bundle()

        bundle.putLong("ID", id)
        bundle.putString("Name", name)
        bundle.putString("Email", email)
        bundle.putString("Number", number)
        bundle.putString("Location", location)
        bundle.putString("Genre", genre)
        bundle.putString("Profession", profession)

        return bundle
    }
}


