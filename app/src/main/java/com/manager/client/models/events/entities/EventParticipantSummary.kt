package com.manager.client.models.events.entities

data class EventParticipantSummary(
    val id: Long,
    val name: String,
    val location: String
)
