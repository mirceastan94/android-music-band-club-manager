package com.manager.client.models.users.dto

data class RegisterUserDTO(
    val name: String,
    val email: String,
    val password: String,
    val number: String,
    val location: String,
    val genre: String,
    val profession: String,
    val language: String
)
