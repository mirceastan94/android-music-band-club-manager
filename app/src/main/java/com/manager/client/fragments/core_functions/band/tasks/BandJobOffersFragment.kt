package com.manager.client.adapters.core_functions.band

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.tasks.BandJobOffersAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.jobs.CreateJobOfferDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BAND
import com.manager.client.utils.BLANK
import com.manager.client.utils.MediaPlayerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandJobOffersFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandJobOffersRecyclerView: RecyclerView

    lateinit var bandJobOffersAdapter: BandJobOffersAdapter

    lateinit var bandJobOfferExperienceSpinner: Spinner
    lateinit var bandJobOfferProfessionSpinner: Spinner

    lateinit var createBandJobOfferButton: TextView

    var bandJobOfferDescriptionText: EditText? = null
    var bandJobOfferLocationText: EditText? = null

    lateinit var userBandName: String

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        val userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {
            userBandName = userBandDetails.name

            bandRootView =
                inflater!!.inflate(R.layout.fragment_band_tasks_job_offers, container, false)

            // sets the profession spinner adapter
            bandJobOfferProfessionSpinner =
                bandRootView.findViewById(R.id.bandJobOfferProfessionSpinner) as Spinner
            val professionSpinnerAdapter = ArrayAdapter.createFromResource(
                activity,
                R.array.user_profession,
                R.layout.spinner_item
            )
            professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            bandJobOfferProfessionSpinner.adapter = professionSpinnerAdapter
            bandJobOfferProfessionSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }

            // sets the experience spinner adapter
            bandJobOfferExperienceSpinner =
                bandRootView.findViewById(R.id.bandJobOfferExperienceSpinner) as Spinner
            val experienceSpinnerAdapter = ArrayAdapter.createFromResource(
                activity,
                R.array.user_experience,
                R.layout.spinner_item
            )
            experienceSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            bandJobOfferExperienceSpinner.adapter = experienceSpinnerAdapter
            bandJobOfferExperienceSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }

            bandJobOfferDescriptionText =
                bandRootView.findViewById(R.id.bandJobOfferDescriptionText) as EditText
            bandJobOfferLocationText =
                bandRootView.findViewById(R.id.bandJobOfferLocationText) as EditText

            createBandJobOfferButton =
                bandRootView.findViewById(R.id.createBandJobOfferButton) as TextView

            bandJobOffersRecyclerView = bandRootView.findViewById(R.id.bandJobOffersRecyclerView)
            bandJobOffersAdapter = BandJobOffersAdapter()

            bandJobOffersRecyclerView.layoutManager = LinearLayoutManager(bandRootView.context)
            bandJobOffersRecyclerView.adapter = bandJobOffersAdapter

            // handles Register button press
            createBandJobOfferButton.setOnClickListener {
                if (!validateUserInput())
                    onSignupFailed()
                else {
                    createNewBandJobOffer()
                }
            }

        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
        }
        return bandRootView
    }

    /**
     * Validates the user data input before adding the new band job offer
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val location = bandJobOfferLocationText!!.text.toString()
        val description = bandJobOfferDescriptionText!!.text.toString()

        if (location.isBlank()) {
            bandJobOfferLocationText!!.error =
                resources.getString(R.string.band_job_offer_location_error_text)
            validUserData = false
        } else {
            bandJobOfferLocationText!!.error = null
        }

        if (description.isEmpty() || description.length < 5) {
            bandJobOfferDescriptionText!!.error =
                resources.getString(R.string.band_job_offer_description_error_text)
            validUserData = false
        } else {
            bandJobOfferDescriptionText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onSignupFailed() {
        MediaPlayerManager.play(bandRootView.context, R.raw.sound_error)
    }

    /**
     * Implements the band job offer creation process
     */
    fun createNewBandJobOffer() {

        val newRequest = CreateJobOfferDTO(
            BAND,
            bandJobOfferProfessionSpinner.selectedItem.toString(),
            bandJobOfferLocationText!!.text.toString(),
            bandJobOfferExperienceSpinner.selectedItem.toString(),
            bandJobOfferDescriptionText!!.text.toString(),
            userBandName,
            BLANK
        )

        val retrofitInstance = JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
            ?.create(ManagerAPI::class.java)
        val createBandJobOfferRequest = retrofitInstance?.createJobOffer(newRequest)

        createBandJobOfferRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<APIResponseDTO>?,
                apiResponseDTO: Response<APIResponseDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), newRequest.toString())
                    bandJobOffersAdapter = BandJobOffersAdapter()
                    bandJobOffersRecyclerView.adapter = bandJobOffersAdapter
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string());
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught at band job offers", throwable.toString())
            }
        })
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandJobOffersFragment) :
            AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                        ?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

}