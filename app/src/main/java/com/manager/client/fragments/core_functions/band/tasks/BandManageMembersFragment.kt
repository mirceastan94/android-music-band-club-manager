package com.manager.client.adapters.core_functions.band

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.tasks.BandManageMembersAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.BandAddMemberActivity

class BandManageMembersFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandManageMembersRecyclerView: RecyclerView
    lateinit var bandRefreshImageView: ImageView
    lateinit var bandManageMembersFab: FloatingActionButton

    lateinit var manageBandMembersAdapter: BandManageMembersAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {
            bandRootView = inflater!!.inflate(R.layout.fragment_band_tasks_manage_members, container, false)
            bandManageMembersRecyclerView = bandRootView.findViewById(R.id.bandManageMembersRecyclerView)
            bandRefreshImageView = bandRootView.findViewById(R.id.bandRefreshImageView)
            bandManageMembersFab = bandRootView.findViewById(R.id.bandManageMembersFab)

            manageBandMembersAdapter = BandManageMembersAdapter()

            bandManageMembersRecyclerView.layoutManager = LinearLayoutManager(bandRootView.context)
            bandManageMembersRecyclerView.adapter = manageBandMembersAdapter

            bandManageMembersFab.setOnClickListener {
                val detailsIntent = Intent(bandRootView.context, BandAddMemberActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                bandRootView.context.startActivity(detailsIntent)
            }

            bandRefreshImageView.setOnClickListener {
                manageBandMembersAdapter = BandManageMembersAdapter()
                bandManageMembersRecyclerView.layoutManager = LinearLayoutManager(bandRootView.context)
                bandManageMembersRecyclerView.adapter = manageBandMembersAdapter
            }

        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(bandRootView.context, R.string.band_update_no_band_created_text, Toast.LENGTH_LONG).show()
        }
        return bandRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandManageMembersFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

}