package com.manager.client.adapters.core_functions.club

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubManagementTasksAdapter

class ClubTasksMainFragment : Fragment() {

    lateinit var tasksAdapter: ClubManagementTasksAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_club_tasks_main, container, false)
        val clubTasksTabLayout = rootView.findViewById(R.id.clubTasksTabLayout) as TabLayout
        val clubTasksViewPager = rootView.findViewById(R.id.clubTasksViewPager) as ViewPager
        tasksAdapter = ClubManagementTasksAdapter(activity, childFragmentManager, clubTasksTabLayout!!.tabCount)
        clubTasksViewPager!!.adapter = tasksAdapter

        clubTasksViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(clubTasksTabLayout))

        clubTasksTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                clubTasksViewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
        return rootView

    }
}