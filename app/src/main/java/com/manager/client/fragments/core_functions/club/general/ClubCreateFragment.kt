package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.club.dto.CreateClubDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.mapMusicGenresToSongSamples
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ClubCreateFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubGenreSpinner: Spinner
    lateinit var clubNameText: TextView
    lateinit var clubLocationText: TextView
    lateinit var clubStreetText: TextView
    lateinit var createClubButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(clubRootView.context, R.string.club_create_already_created_text, Toast.LENGTH_LONG).show()
        } else {
            // initialize the layout components

            val samplesMap = mapMusicGenresToSongSamples()

            clubRootView = inflater!!.inflate(R.layout.fragment_club_general_create, container, false)
            clubGenreSpinner = clubRootView.findViewById(R.id.createClubGenreSpinner)
            clubNameText = clubRootView.findViewById(R.id.createClubNameText)
            clubStreetText = clubRootView.findViewById(R.id.createClubStreetText)
            clubLocationText = clubRootView.findViewById(R.id.createClubLocationText)
            createClubButton = clubRootView.findViewById(R.id.createClubButton)
            // sets the music genre spinner adapter
            val spinnerAdapter = ArrayAdapter.createFromResource(activity, R.array.music_genres, R.layout.spinner_item)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            var spinnerFirstSelection = true
            clubGenreSpinner?.adapter = spinnerAdapter

            // plays the song sample based on the selected genre
            clubGenreSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (!spinnerFirstSelection) {
                        val selectedGenre = clubGenreSpinner?.selectedItem.toString()
                        MediaPlayerManager.play(activity, samplesMap.get(selectedGenre)!!)
                    }
                    spinnerFirstSelection = false
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            // handles Register button press
            createClubButton.setOnClickListener {
                if (!validateClubInput())
                    onCreateFailed()
                else {
                    createClub()
                }
            }
        }
        return clubRootView

    }

    /**
     * Validates the data input before beginning the club creation process
     */
    fun validateClubInput(): Boolean {
        var validUserData = true

        val name = clubNameText!!.text.toString()
        val location = clubLocationText!!.text.toString()
        val street = clubStreetText!!.text.toString()

        if (name.isEmpty() || name.length < 6) {
            clubNameText!!.error = resources.getString(R.string.club_name_error_text)
            validUserData = false
        } else {
            clubNameText!!.error = null
        }
        if (location.isBlank()) {
            clubLocationText!!.error = resources.getString(R.string.club_location_error_text)
            validUserData = false
        } else {
            clubLocationText!!.error = null
        }
        if (location.isBlank()) {
            clubStreetText!!.error = resources.getString(R.string.club_street_error_text)
            validUserData = false
        } else {
            clubStreetText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onCreateFailed() {
        MediaPlayerManager.play(activity, R.raw.sound_error)
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubCreateFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

    /**
     * Implements the club creation process
     */
    fun createClub() {
        val createClubProgressBar = clubRootView.findViewById(R.id.createClubProgressBar) as ProgressBar
        val createClubProgressText = clubRootView.findViewById(R.id.createClubProgressText) as TextView
        val createClubButtonLayout = clubRootView.findViewById(R.id.createClubButtonLayout) as ConstraintLayout
        val createClubProgressLayout = clubRootView.findViewById(R.id.createClubProgressLayout) as ConstraintLayout

        createClubProgressText.setText(R.string.club_create_in_progress_text)
        createClubButtonLayout.setVisibility(View.INVISIBLE)
        createClubProgressLayout.setVisibility(View.VISIBLE)

        val newClubDTO = CreateClubDTO(
            clubNameText!!.text.toString(),
            clubLocationText!!.text.toString(),
            clubStreetText!!.text.toString(),
            clubGenreSpinner.selectedItem.toString()
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val createClubRequest = retrofitInstance?.createClub(newClubDTO)

        createClubRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), newClubDTO.toString())
                    createClubProgressText.setText(R.string.club_create_successful_text)
                    createClubProgressBar.setVisibility(View.INVISIBLE)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                    createClubProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiResponseDTO?.code()) {
                        400 -> createClubProgressText.setText(R.string.club_create_failed_name_exists_text)
                        else -> createClubProgressText.setText(R.string.club_create_failed_internal_error_text)
                    }
                    createClubButtonLayout.setVisibility(View.VISIBLE)
                    createClubProgressLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
                createClubButtonLayout.setVisibility(View.VISIBLE)
                createClubProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

}