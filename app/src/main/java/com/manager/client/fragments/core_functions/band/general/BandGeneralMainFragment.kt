package com.manager.client.adapters.core_functions.band

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.general.BandManagementGeneralAdapter

class BandGeneralMainFragment : Fragment() {

    lateinit var generalAdapter: BandManagementGeneralAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_band_general_main, container, false)
        val bandGeneralTabLayout = rootView.findViewById(R.id.bandGeneralTabLayout) as TabLayout
        val bandGeneralViewPager = rootView.findViewById(R.id.bandGeneralViewPager) as ViewPager
        generalAdapter = BandManagementGeneralAdapter(activity, childFragmentManager, bandGeneralTabLayout!!.tabCount)
        bandGeneralViewPager!!.adapter = generalAdapter

        bandGeneralViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(bandGeneralTabLayout))

        bandGeneralTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                bandGeneralViewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
        return rootView

    }

}