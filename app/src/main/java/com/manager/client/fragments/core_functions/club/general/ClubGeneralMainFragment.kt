package com.manager.client.adapters.core_functions.club

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.general.ClubManagementGeneralAdapter

class ClubGeneralMainFragment : Fragment() {

    lateinit var generalAdapter: ClubManagementGeneralAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_club_general_main, container, false)
        val clubGeneralTabLayout = rootView.findViewById(R.id.clubGeneralTabLayout) as TabLayout
        val clubGeneralViewPager = rootView.findViewById(R.id.clubGeneralViewPager) as ViewPager
        generalAdapter = ClubManagementGeneralAdapter(activity, childFragmentManager, clubGeneralTabLayout!!.tabCount)
        clubGeneralViewPager!!.adapter = generalAdapter

        clubGeneralViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(clubGeneralTabLayout))

        clubGeneralTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                clubGeneralViewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
        return rootView

    }

}