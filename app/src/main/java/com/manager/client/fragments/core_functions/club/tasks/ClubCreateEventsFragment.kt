package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubCreateEventsAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.events.dto.CreateEventDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.LINE
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.ZERO
import kotlinx.android.synthetic.main.fragment_club_tasks_create_events.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.stream.Collectors


class ClubCreateEventsFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubCreateEventsRecyclerView: RecyclerView

    lateinit var clubCreateEventsAdapter: ClubCreateEventsAdapter

    lateinit var clubCreateEventButton: TextView

    lateinit var clubCreateEventsBandNameSpinner: Spinner
    lateinit var clubCreateEventsCalendarView: CalendarView
    lateinit var clubCreateEventsTimePicker: TimePicker
    lateinit var clubCreateEventsCapacityText: EditText
    lateinit var clubCreateEventsDescriptionText: EditText

    var clubCreateEventDate: LocalDate = LocalDate.now().plusDays(1)
    var clubCreateEventTime: LocalTime = LocalTime.of(20, 0)

    lateinit var userClubName: String

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club and gets all the band details to populate the spinner
        val getUserClub = GetUserClub(this)
        val userClubDetails = getUserClub.execute().get()
        val getAllBands = GetAllBands(this)
        val availableBands = getAllBands.execute().get()

        if (userClubDetails != null) {
            userClubName = userClubDetails.name

            clubRootView = inflater!!.inflate(R.layout.fragment_club_tasks_create_events, container, false)

            clubCreateEventsCapacityText = clubRootView.findViewById(R.id.clubCreateEventsCapacityText) as EditText
            clubCreateEventsDescriptionText = clubRootView.findViewById(R.id.clubCreateEventsDescriptionText) as EditText

            clubCreateEventButton = clubRootView.findViewById(R.id.clubCreateEventButton) as TextView

            clubCreateEventsRecyclerView = clubRootView.findViewById(R.id.clubCreateEventsRecyclerView)
            clubCreateEventsAdapter = ClubCreateEventsAdapter()

            clubCreateEventsRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
            clubCreateEventsRecyclerView.adapter = clubCreateEventsAdapter

            // sets the bands list spinner adapter
            var bandsNameList = availableBands.stream().map { currentBandDetails -> currentBandDetails.name }.collect(Collectors.toList())
            val professionSpinnerAdapter = ArrayAdapter<String>(activity, R.layout.spinner_item, bandsNameList)
            professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)

            clubCreateEventsBandNameSpinner = clubRootView.findViewById(R.id.clubCreateEventsBandNameSpinner) as Spinner
            clubCreateEventsBandNameSpinner.adapter = professionSpinnerAdapter
            clubCreateEventsBandNameSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {}
                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            // sets the event date
            clubCreateEventsCalendarView = clubRootView.findViewById(R.id.clubCreateEventsCalendarView) as CalendarView

            val clubCreateEventsDateButton: TextView? = clubRootView.findViewById(R.id.clubCreateEventsDateButton) as TextView
            clubCreateEventsDateButton?.setOnClickListener {
                clubCreateEventsCalendarView.setMinDate(System.currentTimeMillis() - 1000)
                clubCreateEventsCalendarLayout.setVisibility(View.VISIBLE)
                clubCreateEventsInputLayout.setVisibility(View.INVISIBLE)
            }

            clubCreateEventsCalendarView.setOnDateChangeListener { _, year, month, dayOfMonth ->
                var dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE
                var formattedMonth: String
                var formattedDay: String
                if (month >= 9) {
                    formattedMonth = (month + 1).toString()
                } else {
                    formattedMonth = ZERO.plus(month + 1)
                }
                if (dayOfMonth < 10) {
                    formattedDay = ZERO.plus(dayOfMonth)
                } else {
                    formattedDay = dayOfMonth.toString()
                }
                clubCreateEventDate = LocalDate.parse(year.toString().plus(LINE).plus(formattedMonth).plus(LINE).plus(formattedDay), dateTimeFormatter)
            }

            val clubCreateEventChooseDateButton: TextView? = clubRootView.findViewById(R.id.clubCreateEventsChooseDateButton) as TextView
            clubCreateEventChooseDateButton?.setOnClickListener {
                clubCreateEventsCalendarLayout.setVisibility(View.INVISIBLE)
                clubCreateEventsInputLayout.setVisibility(View.VISIBLE)
            }

            // sets the event time
            clubCreateEventsTimePicker = clubRootView.findViewById(R.id.clubCreateEventsTimePicker) as TimePicker

            val clubCreateEventsTimeButton: TextView? = clubRootView.findViewById(R.id.clubCreateEventsTimeButton) as TextView
            clubCreateEventsTimeButton?.setOnClickListener {
                clubCreateEventsTimeLayout.setVisibility(View.VISIBLE)
                clubCreateEventsInputLayout.setVisibility(View.INVISIBLE)
            }

            clubCreateEventsTimePicker.setIs24HourView(true)
            clubCreateEventsTimePicker.setOnTimeChangedListener { _, hourOfDay, minute ->
                clubCreateEventTime = LocalTime.of(hourOfDay.toInt(), minute.toInt())
            }

            val clubCreateEventsChooseTimeButton: TextView? = clubRootView.findViewById(R.id.clubCreateEventsChooseTimeButton) as TextView
            clubCreateEventsChooseTimeButton?.setOnClickListener {
                clubCreateEventsTimeLayout.setVisibility(View.INVISIBLE)
                clubCreateEventsInputLayout.setVisibility(View.VISIBLE)
            }

            // validates user input then creates event
            clubCreateEventButton.setOnClickListener {
                if (!validateUserInput())
                    onSignupFailed()
                else {
                    createNewEvent()
                }
            }

        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
        }
        return clubRootView
    }

    /**
     * Validates the user data input before creating the new event
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val eventCapacity = clubCreateEventsCapacityText!!.text.toString()
        val eventDescription = clubCreateEventsDescriptionText!!.text.toString()

        if (eventCapacity.isBlank() || eventCapacity.toInt() < 5) {
            clubCreateEventsCapacityText!!.error = resources.getString(R.string.event_capacity_error_text)
            validUserData = false
        } else {
            clubCreateEventsCapacityText!!.error = null
        }

        if (eventDescription.isEmpty() || eventDescription.length < 5) {
            clubCreateEventsDescriptionText!!.error = resources.getString(R.string.club_job_offer_description_error_text)
            validUserData = false
        } else {
            clubCreateEventsDescriptionText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onSignupFailed() {
        MediaPlayerManager.play(clubRootView.context, R.raw.sound_error)
    }

    /**
     * Implements the event creation process
     */
    fun createNewEvent() {
        val newEvent = CreateEventDTO(
            userClubName,
            clubCreateEventsBandNameSpinner!!.selectedItem.toString(),
            clubCreateEventsCapacityText!!.text.toString().toInt(),
            clubCreateEventDate.toString(),
            clubCreateEventTime.toString(),
            clubCreateEventsDescriptionText!!.text.toString()
        )

        val retrofitInstance = JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val createEventRequest = retrofitInstance?.createEvent(newEvent)

        createEventRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), newEvent.toString())
                    clubCreateEventsAdapter = ClubCreateEventsAdapter()
                    clubCreateEventsRecyclerView.adapter = clubCreateEventsAdapter
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    if (errorResponse.message.contains("Another event at the same club and date time exists")) {
                        Toast.makeText(activity.applicationContext, R.string.event_already_exists_alert_text, Toast.LENGTH_LONG).show()
                        MediaPlayerManager.play(activity.applicationContext, R.raw.sound_error)
                    }
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught at event creation", throwable.toString())
            }
        })
    }

    /**
     * Handles the synchronous calls to determine all the available bands and if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubCreateEventsFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }

        class GetAllBands internal constructor(context: ClubCreateEventsFragment) : AsyncTask<Void, Void, List<BandDetailsDTO>>() {
            override fun doInBackground(vararg params: Void?): List<BandDetailsDTO>? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getAllBands = retrofitInstance?.getAllBands()
                var response = getAllBands?.execute()
                val allBandsDetails: List<BandDetailsDTO>?
                if (response!!.isSuccessful) {
                    allBandsDetails = response?.body()!!.bandSummaryList
                } else {
                    allBandsDetails = null
                }
                return allBandsDetails
            }
        }

    }

}