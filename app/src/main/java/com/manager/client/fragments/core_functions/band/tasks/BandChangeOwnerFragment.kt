package com.manager.client.adapters.core_functions.band

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.tasks.BandChangeOwnerAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit

class BandChangeOwnerFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandChangeOwnerRecyclerView: RecyclerView

    lateinit var bandChangeOwnerAdapter: BandChangeOwnerAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {

            bandRootView = inflater!!.inflate(R.layout.fragment_band_tasks_change_owner, container, false)
            bandChangeOwnerRecyclerView = bandRootView.findViewById(R.id.bandChangeOwnerRecyclerView)
            bandChangeOwnerAdapter = BandChangeOwnerAdapter()

            bandChangeOwnerRecyclerView.layoutManager = LinearLayoutManager(bandRootView.context)
            bandChangeOwnerRecyclerView.adapter = bandChangeOwnerAdapter

        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
        }
        return bandRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandChangeOwnerFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

}