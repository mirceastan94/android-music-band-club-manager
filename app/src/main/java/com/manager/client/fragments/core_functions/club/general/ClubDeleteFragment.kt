package com.manager.client.adapters.core_functions.club

import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.MainActivity
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.MediaPlayerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubDeleteFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubNameText: TextView
    lateinit var clubLocationText: TextView
    lateinit var deleteClubButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {

            clubRootView = inflater!!.inflate(R.layout.fragment_club_general_delete, container, false)
            clubNameText = clubRootView.findViewById(R.id.deleteClubNameText)
            clubLocationText = clubRootView.findViewById(R.id.deleteClubLocationText)
            deleteClubButton = clubRootView.findViewById(R.id.deleteClubButton)

            clubNameText.text = userClubDetails.name
            clubLocationText.text = userClubDetails.location

            deleteClubButton?.setOnClickListener {
                MediaPlayerManager.play(clubRootView.context, R.raw.sound_warning);
                val alertBuilder = AlertDialog.Builder(clubRootView.context, R.style.AlertDialogTheme)
                alertBuilder.setMessage(R.string.club_delete_alert_text).setCancelable(true)
                    .setPositiveButton(R.string.alert_yes_text,
                        DialogInterface.OnClickListener { _, _ ->
                            deleteClub()
                        }).setNegativeButton(R.string.alert_no_text,
                        DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })
                val alert = alertBuilder.create()
                alert.show()
            }
        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(clubRootView.context, R.string.club_update_no_club_created_text, Toast.LENGTH_LONG).show()
        }
        return clubRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubDeleteFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

    /**
     * Implements the club update process
     */
    fun deleteClub() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val deleteClubRequest = retrofitInstance?.removeClub()

        deleteClubRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                ActivityHelper.startActivity(activity.applicationContext, MainActivity::class.java)
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
            }
        })
    }

}