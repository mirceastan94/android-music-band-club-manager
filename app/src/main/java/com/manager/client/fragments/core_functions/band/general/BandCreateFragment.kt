package com.manager.client.adapters.core_functions.band

import android.os.AsyncTask
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.band.dto.CreateBandDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.mapMusicGenresToSongSamples
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BandCreateFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandGenreSpinner: Spinner
    lateinit var bandNameText: TextView
    lateinit var bandLocationText: TextView
    lateinit var createBandButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(bandRootView.context, R.string.band_create_already_created_text, Toast.LENGTH_LONG).show()
        } else {
            // initialize the layout components

            val samplesMap = mapMusicGenresToSongSamples()

            bandRootView = inflater!!.inflate(R.layout.fragment_band_general_create, container, false)
            bandGenreSpinner = bandRootView.findViewById(R.id.createBandGenreSpinner)
            bandNameText = bandRootView.findViewById(R.id.createBandNameText)
            bandLocationText = bandRootView.findViewById(R.id.createBandLocationText)
            createBandButton = bandRootView.findViewById(R.id.createBandButton)
            // sets the music genre spinner adapter
            val spinnerAdapter = ArrayAdapter.createFromResource(activity, R.array.music_genres, R.layout.spinner_item)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            var spinnerFirstSelection = true
            bandGenreSpinner?.adapter = spinnerAdapter

            // plays the song sample based on the selected genre
            bandGenreSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (!spinnerFirstSelection) {
                        val selectedGenre = bandGenreSpinner?.selectedItem.toString()
                        MediaPlayerManager.play(activity, samplesMap.get(selectedGenre)!!)
                    }
                    spinnerFirstSelection = false
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            // handles Register button press
            createBandButton.setOnClickListener {
                if (!validateBandInput())
                    onCreateFailed()
                else {
                    createBand()
                }
            }
        }
        return bandRootView

    }

    /**
     * Validates the data input before beginning the band creation process
     */
    fun validateBandInput(): Boolean {
        var validUserData = true

        val name = bandNameText!!.text.toString()
        val location = bandLocationText!!.text.toString()

        if (name.isEmpty() || name.length < 6) {
            bandNameText!!.error = resources.getString(R.string.band_name_error_text)
            validUserData = false
        } else {
            bandNameText!!.error = null
        }
        if (location.isBlank()) {
            bandLocationText!!.error = resources.getString(R.string.band_location_error_text)
            validUserData = false
        } else {
            bandLocationText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onCreateFailed() {
        MediaPlayerManager.play(activity, R.raw.sound_error)
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandCreateFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

    /**
     * Implements the band creation process
     */
    fun createBand() {
        val createBandProgressBar = bandRootView.findViewById(R.id.createBandProgressBar) as ProgressBar
        val createBandProgressText = bandRootView.findViewById(R.id.createBandProgressText) as TextView
        val createBandButtonLayout = bandRootView.findViewById(R.id.createBandButtonLayout) as ConstraintLayout
        val createBandProgressLayout = bandRootView.findViewById(R.id.createBandProgressLayout) as ConstraintLayout

        createBandProgressText.setText(R.string.band_create_in_progress_text)
        createBandButtonLayout.setVisibility(View.INVISIBLE)
        createBandProgressLayout.setVisibility(View.VISIBLE)

        val newBandDTO = CreateBandDTO(
            bandNameText!!.text.toString(),
            bandLocationText!!.text.toString(),
            bandGenreSpinner.selectedItem.toString()
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val createBandRequest = retrofitInstance?.createBand(newBandDTO)

        createBandRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), newBandDTO.toString())
                    createBandProgressText.setText(R.string.band_create_successful_text)
                    createBandProgressBar.setVisibility(View.INVISIBLE)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string());
                    Log.e("Call response", errorResponse.message)
                    createBandProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiResponseDTO?.code()) {
                        400 -> createBandProgressText.setText(R.string.band_create_failed_name_exists_text)
                        else -> createBandProgressText.setText(R.string.band_create_failed_internal_error_text)
                    }
                    createBandButtonLayout.setVisibility(View.VISIBLE)
                    createBandProgressLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
                createBandButtonLayout.setVisibility(View.VISIBLE)
                createBandProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

}