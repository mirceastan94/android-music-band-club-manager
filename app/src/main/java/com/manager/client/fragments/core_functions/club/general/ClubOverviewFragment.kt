package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.general.ClubOverviewMembersAdapter
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit

class ClubOverviewFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubNameText: TextView
    lateinit var clubLocationText: TextView
    lateinit var clubGenreText: TextView
    lateinit var overviewClubMembersRecyclerView: RecyclerView

    lateinit var overviewClubMembersAdapter: ClubOverviewMembersAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {

            clubRootView = inflater!!.inflate(R.layout.fragment_club_general_overview, container, false)
            overviewClubMembersRecyclerView = clubRootView.findViewById(R.id.overviewClubMembersRecyclerView)
            clubGenreText = clubRootView.findViewById(R.id.overviewClubMusicGenreText)
            clubNameText = clubRootView.findViewById(R.id.overviewClubNameText)
            clubLocationText = clubRootView.findViewById(R.id.overviewClubLocationText)

            overviewClubMembersAdapter = ClubOverviewMembersAdapter()

            overviewClubMembersRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
            overviewClubMembersRecyclerView.adapter = overviewClubMembersAdapter

            clubNameText.text = userClubDetails.name
            clubLocationText.text = userClubDetails.location
            clubGenreText.text = userClubDetails.genre
        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(clubRootView.context, R.string.club_update_no_club_created_text, Toast.LENGTH_LONG).show()
        }
        return clubRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubOverviewFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

}