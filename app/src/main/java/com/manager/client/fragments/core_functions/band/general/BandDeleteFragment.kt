package com.manager.client.adapters.core_functions.band

import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.MainActivity
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.MediaPlayerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandDeleteFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandNameText: TextView
    lateinit var bandLocationText: TextView
    lateinit var deleteBandButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {

            bandRootView = inflater!!.inflate(R.layout.fragment_band_general_delete, container, false)
            bandNameText = bandRootView.findViewById(R.id.deleteBandNameText)
            bandLocationText = bandRootView.findViewById(R.id.deleteBandLocationText)
            deleteBandButton = bandRootView.findViewById(R.id.deleteBandButton)

            bandNameText.text = userBandDetails.name
            bandLocationText.text = userBandDetails.location

            deleteBandButton?.setOnClickListener {
                MediaPlayerManager.play(bandRootView.context, R.raw.sound_warning);
                val alertBuilder = AlertDialog.Builder(bandRootView.context, R.style.AlertDialogTheme)
                alertBuilder.setMessage(R.string.band_delete_alert_text).setCancelable(true)
                    .setPositiveButton(R.string.alert_yes_text,
                        DialogInterface.OnClickListener { _, _ ->
                            deleteBand()
                        }).setNegativeButton(R.string.alert_no_text,
                        DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })
                val alert = alertBuilder.create()
                alert.show()
            }
        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(bandRootView.context, R.string.band_update_no_band_created_text, Toast.LENGTH_LONG).show()
        }
        return bandRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandDeleteFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

    /**
     * Implements the band update process
     */
    fun deleteBand() {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val deleteBandRequest = retrofitInstance?.removeBand()

        deleteBandRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                ActivityHelper.startActivity(activity.applicationContext, MainActivity::class.java)
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
            }
        })
    }

}