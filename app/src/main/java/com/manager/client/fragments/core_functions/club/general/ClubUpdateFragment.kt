package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.club.dto.UpdateClubDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.mapMusicGenresToSongSamples
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubUpdateFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubGenreSpinner: Spinner
    lateinit var clubNameText: TextView
    lateinit var clubLocationText: TextView
    lateinit var updateClubButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {

            // initialize the layout components
            val samplesMap = mapMusicGenresToSongSamples()

            clubRootView = inflater!!.inflate(R.layout.fragment_club_general_update, container, false)
            clubGenreSpinner = clubRootView.findViewById(R.id.updateClubGenreSpinner)
            clubNameText = clubRootView.findViewById(R.id.updateClubNameText)
            clubLocationText = clubRootView.findViewById(R.id.updateClubLocationText)
            updateClubButton = clubRootView.findViewById(R.id.updateClubButton)

            clubNameText.text = userClubDetails.name
            clubLocationText.text = userClubDetails.location
            // sets the music genre spinner adapter
            val spinnerAdapter = ArrayAdapter.createFromResource(activity, R.array.music_genres, R.layout.spinner_item)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            var spinnerFirstSelection = true
            clubGenreSpinner?.adapter = spinnerAdapter

            // plays the song sample based on the selected genre
            clubGenreSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (!spinnerFirstSelection) {
                        val selectedGenre = clubGenreSpinner?.selectedItem.toString()
                        MediaPlayerManager.play(activity, samplesMap.get(selectedGenre)!!)
                    }
                    spinnerFirstSelection = false
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            // handles Register button press
            updateClubButton.setOnClickListener {
                if (!validateClubInput())
                    onUpdateFailed()
                else {
                    updateClub(userClubDetails.id)
                }
            }
        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(clubRootView.context, R.string.club_update_no_club_created_text, Toast.LENGTH_LONG).show()
        }
        return clubRootView
    }

    /**
     * Validates the data input before beginning the club creation process
     */
    fun validateClubInput(): Boolean {
        var validUserData = true

        val name = clubNameText!!.text.toString()
        val location = clubLocationText!!.text.toString()

        if (name.isEmpty() || name.length < 6) {
            clubNameText!!.error = resources.getString(R.string.club_name_error_text)
            validUserData = false
        } else {
            clubNameText!!.error = null
        }
        if (location.isBlank()) {
            clubLocationText!!.error = resources.getString(R.string.club_location_error_text)
            validUserData = false
        } else {
            clubLocationText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onUpdateFailed() {
        MediaPlayerManager.play(activity, R.raw.sound_error)
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubUpdateFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

    /**
     * Implements the club update process
     */
    fun updateClub(clubId: Long) {
        val updateClubProgressBar = clubRootView.findViewById(R.id.updateClubProgressBar) as ProgressBar
        val updateClubProgressText = clubRootView.findViewById(R.id.updateClubProgressText) as TextView
        val updateClubButtonLayout = clubRootView.findViewById(R.id.updateClubButtonLayout) as ConstraintLayout
        val updateClubProgressLayout = clubRootView.findViewById(R.id.updateClubProgressLayout) as ConstraintLayout

        updateClubProgressText.setText(R.string.club_update_in_progress_text)
        updateClubButtonLayout.setVisibility(View.INVISIBLE)
        updateClubProgressLayout.setVisibility(View.VISIBLE)

        val updateClubDTO = UpdateClubDTO(
            clubId,
            clubNameText!!.text.toString(),
            clubLocationText!!.text.toString(),
            clubGenreSpinner.selectedItem.toString()
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val updateClubRequest = retrofitInstance?.updateClub(updateClubDTO)

        updateClubRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), updateClubDTO.toString())
                    updateClubProgressText.setText(R.string.club_update_successful_text)
                    updateClubProgressBar.setVisibility(View.INVISIBLE)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string());
                    Log.e("Call response", errorResponse.message)
                    updateClubProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiResponseDTO?.code()) {
                        400 -> updateClubProgressText.setText(R.string.club_update_failed_name_exists_text)
                        else -> updateClubProgressText.setText(R.string.club_update_failed_internal_error_text)
                    }
                    updateClubButtonLayout.setVisibility(View.VISIBLE)
                    updateClubProgressLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
                updateClubButtonLayout.setVisibility(View.VISIBLE)
                updateClubProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

}