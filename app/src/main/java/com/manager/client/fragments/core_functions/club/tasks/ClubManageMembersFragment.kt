package com.manager.client.adapters.core_functions.club

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubManageMembersAdapter
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.ClubAddMemberActivity

class ClubManageMembersFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubManageMembersRecyclerView: RecyclerView
    lateinit var clubRefreshImageView: ImageView
    lateinit var clubManageMembersFab: FloatingActionButton

    lateinit var manageClubMembersAdapter: ClubManageMembersAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {
            clubRootView = inflater!!.inflate(R.layout.fragment_club_tasks_manage_members, container, false)
            clubManageMembersRecyclerView = clubRootView.findViewById(R.id.clubManageMembersRecyclerView)
            clubRefreshImageView = clubRootView.findViewById(R.id.clubRefreshImageView)
            clubManageMembersFab = clubRootView.findViewById(R.id.clubManageMembersFab)

            manageClubMembersAdapter = ClubManageMembersAdapter()

            clubManageMembersRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
            clubManageMembersRecyclerView.adapter = manageClubMembersAdapter

            clubManageMembersFab.setOnClickListener {
                val detailsIntent = Intent(clubRootView.context, ClubAddMemberActivity::class.java)
                detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                clubRootView.context.startActivity(detailsIntent)
            }

            clubRefreshImageView.setOnClickListener {
                manageClubMembersAdapter = ClubManageMembersAdapter()
                clubManageMembersRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
                clubManageMembersRecyclerView.adapter = manageClubMembersAdapter
            }

        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(clubRootView.context, R.string.club_update_no_club_created_text, Toast.LENGTH_LONG).show()
        }
        return clubRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubManageMembersFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

}