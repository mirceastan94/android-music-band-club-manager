package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubChangeOwnerAdapter
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit

class ClubChangeOwnerFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubChangeOwnerRecyclerView: RecyclerView

    lateinit var clubChangeOwnerAdapter: ClubChangeOwnerAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        var userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {

            clubRootView = inflater!!.inflate(R.layout.fragment_club_tasks_change_owner, container, false)
            clubChangeOwnerRecyclerView = clubRootView.findViewById(R.id.clubChangeOwnerRecyclerView)
            clubChangeOwnerAdapter = ClubChangeOwnerAdapter()

            clubChangeOwnerRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
            clubChangeOwnerRecyclerView.adapter = clubChangeOwnerAdapter

        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
        }
        return clubRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubChangeOwnerFragment) : AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

}