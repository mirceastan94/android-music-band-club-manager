package com.manager.client.adapters.core_functions.band

import android.os.AsyncTask
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.band.dto.UpdateBandDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.mapMusicGenresToSongSamples
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandUpdateFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandGenreSpinner: Spinner
    lateinit var bandNameText: TextView
    lateinit var bandLocationText: TextView
    lateinit var updateBandButton: TextView

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {

            // initialize the layout components
            val samplesMap = mapMusicGenresToSongSamples()

            bandRootView = inflater!!.inflate(R.layout.fragment_band_general_update, container, false)
            bandGenreSpinner = bandRootView.findViewById(R.id.updateBandGenreSpinner)
            bandNameText = bandRootView.findViewById(R.id.updateBandNameText)
            bandLocationText = bandRootView.findViewById(R.id.updateBandLocationText)
            updateBandButton = bandRootView.findViewById(R.id.updateBandButton)

            bandNameText.text = userBandDetails.name
            bandLocationText.text = userBandDetails.location
            // sets the music genre spinner adapter
            val spinnerAdapter = ArrayAdapter.createFromResource(activity, R.array.music_genres, R.layout.spinner_item)
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            var spinnerFirstSelection = true
            bandGenreSpinner?.adapter = spinnerAdapter

            // plays the song sample based on the selected genre
            bandGenreSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    if (!spinnerFirstSelection) {
                        val selectedGenre = bandGenreSpinner?.selectedItem.toString()
                        MediaPlayerManager.play(activity, samplesMap.get(selectedGenre)!!)
                    }
                    spinnerFirstSelection = false
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

            // handles Register button press
            updateBandButton.setOnClickListener {
                if (!validateBandInput())
                    onUpdateFailed()
                else {
                    updateBand(userBandDetails.id)
                }
            }
        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(bandRootView.context, R.string.band_update_no_band_created_text, Toast.LENGTH_LONG).show()
        }
        return bandRootView
    }

    /**
     * Validates the data input before beginning the band creation process
     */
    fun validateBandInput(): Boolean {
        var validUserData = true

        val name = bandNameText!!.text.toString()
        val location = bandLocationText!!.text.toString()

        if (name.isEmpty() || name.length < 6) {
            bandNameText!!.error = resources.getString(R.string.band_name_error_text)
            validUserData = false
        } else {
            bandNameText!!.error = null
        }
        if (location.isBlank()) {
            bandLocationText!!.error = resources.getString(R.string.band_location_error_text)
            validUserData = false
        } else {
            bandLocationText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onUpdateFailed() {
        MediaPlayerManager.play(activity, R.raw.sound_error)
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandUpdateFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

    /**
     * Implements the band update process
     */
    fun updateBand(bandId: Long) {
        val updateBandProgressBar = bandRootView.findViewById(R.id.updateBandProgressBar) as ProgressBar
        val updateBandProgressText = bandRootView.findViewById(R.id.updateBandProgressText) as TextView
        val updateBandButtonLayout = bandRootView.findViewById(R.id.updateBandButtonLayout) as ConstraintLayout
        val updateBandProgressLayout = bandRootView.findViewById(R.id.updateBandProgressLayout) as ConstraintLayout

        updateBandProgressText.setText(R.string.band_update_in_progress_text)
        updateBandButtonLayout.setVisibility(View.INVISIBLE)
        updateBandProgressLayout.setVisibility(View.VISIBLE)

        val updateBandDTO = UpdateBandDTO(
            bandId,
            bandNameText!!.text.toString(),
            bandLocationText!!.text.toString(),
            bandGenreSpinner.selectedItem.toString()
        )

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val updateBandRequest = retrofitInstance?.updateBand(updateBandDTO)

        updateBandRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), updateBandDTO.toString())
                    updateBandProgressText.setText(R.string.band_update_successful_text)
                    updateBandProgressBar.setVisibility(View.INVISIBLE)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string());
                    Log.e("Call response", errorResponse.message)
                    updateBandProgressText.setTextSize(
                        TypedValue.COMPLEX_UNIT_SP,
                        R.dimen.create_update_button_failed_text.toFloat()
                    )
                    when (apiResponseDTO?.code()) {
                        400 -> updateBandProgressText.setText(R.string.band_update_failed_name_exists_text)
                        else -> updateBandProgressText.setText(R.string.band_update_failed_internal_error_text)
                    }
                    updateBandButtonLayout.setVisibility(View.VISIBLE)
                    updateBandProgressLayout.setVisibility(View.INVISIBLE)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught", throwable.toString())
                updateBandButtonLayout.setVisibility(View.VISIBLE)
                updateBandProgressLayout.setVisibility(View.INVISIBLE)
            }
        })
    }

}