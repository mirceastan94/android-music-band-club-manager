package com.manager.client.adapters.core_functions.band

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.general.BandOverviewMembersAdapter
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit

class BandOverviewFragment : Fragment() {

    lateinit var bandRootView: View
    lateinit var bandNameText: TextView
    lateinit var bandLocationText: TextView
    lateinit var bandGenreText: TextView
    lateinit var overviewBandMembersRecyclerView: RecyclerView

    lateinit var overviewBandMembersAdapter: BandOverviewMembersAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a band
        val getUserBand = GetUserBand(this)
        var userBandDetails = getUserBand.execute().get()

        if (userBandDetails != null) {

            bandRootView = inflater!!.inflate(R.layout.fragment_band_general_overview, container, false)
            overviewBandMembersRecyclerView = bandRootView.findViewById(R.id.overviewBandMembersRecyclerView)
            bandGenreText = bandRootView.findViewById(R.id.overviewBandMusicGenreText)
            bandNameText = bandRootView.findViewById(R.id.overviewBandNameText)
            bandLocationText = bandRootView.findViewById(R.id.overviewBandLocationText)

            overviewBandMembersAdapter = BandOverviewMembersAdapter()

            overviewBandMembersRecyclerView.layoutManager = LinearLayoutManager(bandRootView.context)
            overviewBandMembersRecyclerView.adapter = overviewBandMembersAdapter

            bandNameText.text = userBandDetails.name
            bandLocationText.text = userBandDetails.location
            bandGenreText.text = userBandDetails.genre
        } else {
            bandRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
            Toast.makeText(bandRootView.context, R.string.band_update_no_band_created_text, Toast.LENGTH_LONG).show()
        }
        return bandRootView
    }

    /**
     * Handles the synchronous call to determine if the user has already created a band
     */
    companion object {
        class GetUserBand internal constructor(context: BandOverviewFragment) : AsyncTask<Void, Void, BandDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): BandDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
                val getCurrentBand = retrofitInstance?.getBand()
                var response = getCurrentBand?.execute()
                val currentBandDetailsDTO: BandDetailsDTO?
                if (response!!.isSuccessful) {
                    currentBandDetailsDTO = response?.body()!!
                } else {
                    currentBandDetailsDTO = null
                }
                return currentBandDetailsDTO
            }
        }
    }

}