package com.manager.client.adapters.core_functions.band

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.manager.client.R
import com.manager.client.adapters.core_functions.band.tasks.BandManagementTasksAdapter

class BandTasksMainFragment : Fragment() {

    lateinit var tasksAdapter: BandManagementTasksAdapter

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_band_tasks_main, container, false)
        val bandTasksTabLayout = rootView.findViewById(R.id.bandTasksTabLayout) as TabLayout
        val bandTasksViewPager = rootView.findViewById(R.id.bandTasksViewPager) as ViewPager
        tasksAdapter = BandManagementTasksAdapter(activity, childFragmentManager, bandTasksTabLayout!!.tabCount)
        bandTasksViewPager!!.adapter = tasksAdapter

        bandTasksViewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(bandTasksTabLayout))

        bandTasksTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                bandTasksViewPager!!.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
        return rootView

    }
}