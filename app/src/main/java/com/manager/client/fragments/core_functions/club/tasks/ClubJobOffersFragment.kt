package com.manager.client.adapters.core_functions.club

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.adapters.core_functions.club.tasks.ClubJobOffersAdapter
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.jobs.CreateJobOfferDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BLANK
import com.manager.client.utils.CLUB
import com.manager.client.utils.MediaPlayerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubJobOffersFragment : Fragment() {

    lateinit var clubRootView: View
    lateinit var clubJobOffersRecyclerView: RecyclerView

    lateinit var clubJobOffersAdapter: ClubJobOffersAdapter

    lateinit var clubJobOfferExperienceSpinner: Spinner
    lateinit var clubJobOfferProfessionSpinner: Spinner

    lateinit var createClubJobOfferButton: TextView

    var clubJobOfferDescriptionText: EditText? = null
    var clubJobOfferLocationText: EditText? = null

    lateinit var userClubName: String

    override fun onCreateView(
        inflater: LayoutInflater?, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // check if the user already created a club
        val getUserClub = GetUserClub(this)
        val userClubDetails = getUserClub.execute().get()

        if (userClubDetails != null) {
            userClubName = userClubDetails.name

            clubRootView =
                inflater!!.inflate(R.layout.fragment_club_tasks_job_offers, container, false)

            // sets the profession spinner adapter
            clubJobOfferProfessionSpinner =
                clubRootView.findViewById(R.id.clubJobOfferProfessionSpinner) as Spinner
            val professionSpinnerAdapter = ArrayAdapter.createFromResource(
                activity,
                R.array.club_profession,
                R.layout.spinner_item
            )
            professionSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            clubJobOfferProfessionSpinner.adapter = professionSpinnerAdapter
            clubJobOfferProfessionSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }

            // sets the experience spinner adapter
            clubJobOfferExperienceSpinner =
                clubRootView.findViewById(R.id.clubJobOfferExperienceSpinner) as Spinner
            val experienceSpinnerAdapter = ArrayAdapter.createFromResource(
                activity,
                R.array.user_experience,
                R.layout.spinner_item
            )
            experienceSpinnerAdapter.setDropDownViewResource(R.layout.spinner_item_drop)
            clubJobOfferExperienceSpinner.adapter = experienceSpinnerAdapter
            clubJobOfferExperienceSpinner.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                }

            clubJobOfferDescriptionText =
                clubRootView.findViewById(R.id.clubJobOfferDescriptionText) as EditText
            clubJobOfferLocationText =
                clubRootView.findViewById(R.id.clubJobOfferLocationText) as EditText

            createClubJobOfferButton =
                clubRootView.findViewById(R.id.createClubJobOfferButton) as TextView

            clubJobOffersRecyclerView = clubRootView.findViewById(R.id.clubJobOffersRecyclerView)
            clubJobOffersAdapter = ClubJobOffersAdapter()

            clubJobOffersRecyclerView.layoutManager = LinearLayoutManager(clubRootView.context)
            clubJobOffersRecyclerView.adapter = clubJobOffersAdapter

            // handles Register button press
            createClubJobOfferButton.setOnClickListener {
                if (!validateUserInput())
                    onSignupFailed()
                else {
                    createNewClubJobOffer()
                }
            }

        } else {
            clubRootView = inflater!!.inflate(R.layout.fragment_empty, container, false)
        }
        return clubRootView
    }

    /**
     * Validates the user data input before adding the new club job offer
     */
    fun validateUserInput(): Boolean {
        var validUserData = true

        val location = clubJobOfferLocationText!!.text.toString()
        val description = clubJobOfferDescriptionText!!.text.toString()

        if (location.isBlank()) {
            clubJobOfferLocationText!!.error =
                resources.getString(R.string.club_job_offer_location_error_text)
            validUserData = false
        } else {
            clubJobOfferLocationText!!.error = null
        }

        if (description.isEmpty() || description.length < 5) {
            clubJobOfferDescriptionText!!.error =
                resources.getString(R.string.club_job_offer_description_error_text)
            validUserData = false
        } else {
            clubJobOfferDescriptionText!!.error = null
        }

        return validUserData
    }

    /**
     * Calls the error sound
     */
    fun onSignupFailed() {
        MediaPlayerManager.play(clubRootView.context, R.raw.sound_error)
    }

    /**
     * Implements the club job offer creation process
     */
    fun createNewClubJobOffer() {

        val newRequest = CreateJobOfferDTO(
            CLUB,
            clubJobOfferProfessionSpinner.selectedItem.toString(),
            clubJobOfferLocationText!!.text.toString(),
            clubJobOfferExperienceSpinner.selectedItem.toString(),
            clubJobOfferDescriptionText!!.text.toString(),
            BLANK,
            userClubName
        )

        val retrofitInstance = JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
            ?.create(ManagerAPI::class.java)
        val createClubJobOfferRequest = retrofitInstance?.createJobOffer(newRequest)

        createClubJobOfferRequest?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<APIResponseDTO>?,
                apiResponseDTO: Response<APIResponseDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), newRequest.toString())
                    clubJobOffersAdapter = ClubJobOffersAdapter()
                    clubJobOffersRecyclerView.adapter = clubJobOffersAdapter
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught at club job offers", throwable.toString())
            }
        })
    }

    /**
     * Handles the synchronous call to determine if the user has already created a club
     */
    companion object {
        class GetUserClub internal constructor(context: ClubJobOffersFragment) :
            AsyncTask<Void, Void, ClubDetailsDTO>() {
            override fun doInBackground(vararg params: Void?): ClubDetailsDTO? {
                val retrofitInstance =
                    JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                        ?.create(ManagerAPI::class.java)
                val getCurrentClub = retrofitInstance?.getClub()
                var response = getCurrentClub?.execute()
                val currentClubDetailsDTO: ClubDetailsDTO?
                if (response!!.isSuccessful) {
                    currentClubDetailsDTO = response?.body()!!
                } else {
                    currentClubDetailsDTO = null
                }
                return currentClubDetailsDTO
            }
        }
    }

}