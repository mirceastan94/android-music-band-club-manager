package com.manager.client.adapters.core_functions.band.general

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandMemberListDTO
import com.manager.client.models.band.entities.BandMemberSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BULLET_DOT
import kotlinx.android.synthetic.main.band_member_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandOverviewMembersAdapter : RecyclerView.Adapter<BandOverviewMembersAdapter.BandMembersViewHolder>() {

    var users: ArrayList<BandMemberSummary> = ArrayList()

    init {
        getAllBandMembers()
    }

    class BandMembersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BandMembersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.band_member_list_item, parent, false)

        return BandMembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: BandMembersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 12) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 13).replaceRange(11, 13, "...")
        }
        holder.view.profession.text = BULLET_DOT.plus(users[position].profession)
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list from the database
     */
    fun getAllBandMembers() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllBandMembersRequest = retrofitInstance?.getMyBandMembers()

        getAllBandMembersRequest?.enqueue(object : Callback<BandMemberListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<BandMemberListDTO>?, apiResponseDTO: Response<BandMemberListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: BandMemberListDTO? = apiResponseDTO.body()
                    users = userList!!.memberSummaryList as ArrayList<BandMemberSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<BandMemberListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }
}