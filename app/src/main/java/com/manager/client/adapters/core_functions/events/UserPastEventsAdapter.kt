package com.manager.client.adapters.core_functions.events

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.EventDetailsDTO
import com.manager.client.models.events.dto.EventListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.EventsDetailsActivity
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.events_user_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserPastEventsAdapter(context: Context) : RecyclerView.Adapter<UserPastEventsAdapter.EventsViewHolder>() {

    var allEvents: ArrayList<EventDetailsDTO> = ArrayList()

    init {
        getPastEvents(context)
    }

    class EventsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventsViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.events_user_list_item, parent, false)

        return EventsViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        // sets events information within the list
        holder.view.clubNameLogo.text = allEvents[position].clubName[0].toString()
        holder.view.clubName.text = allEvents[position].clubName.plus(COMMA)
        holder.view.bandName.text = allEvents[position].bandName
        holder.view.location.text = BULLET_DOT.plus(allEvents[position].location)
        holder.view.dateTime.text = ARROW_WITH_SPACE.plus(allEvents[position].dateTime.replace(T, COMMA))

        holder.view.clubNameLogo.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.clubName.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.bandName.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.location.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.dateTime.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.eventRemoveButton.setOnClickListener { showDeleteClubEventDialog(holder.view, allEvents[position]) }
    }

    override fun getItemCount() = allEvents.size

    /**
     * Retrieves the past events list for the current user
     */
    fun getPastEvents(context: Context) {

        allEvents.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        var getAllPastEventsRequest = retrofitInstance?.getPastEvents()
        getAllPastEventsRequest?.enqueue(object : Callback<EventListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventListDTO>?, apiResponseDTO: Response<EventListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferList: EventListDTO? = apiResponseDTO.body()
                    allEvents = clubJobOfferList!!.eventSummaryList as ArrayList<EventDetailsDTO>
                    if (allEvents.isEmpty()) {
                        Toast.makeText(context, R.string.event_no_past_events_found_error_text, Toast.LENGTH_LONG).show()
                    }
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected event details from the database
     */
    fun getEventDetails(view: View, event: EventDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getEventDetails = retrofitInstance?.getSelectedEventDetails(event.id)
        getEventDetails?.enqueue(object : Callback<EventDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventDetailsDTO>?, apiResponseDTO: Response<EventDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventDetailsDTO: EventDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, EventsDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOver", true)
                    detailsIntent.putExtra("eventDetails", eventDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Removes the selected event from the user's list in the database
     */
    fun deleteEvent(view: View, eventDetailsDTO: EventDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteEventParticipant(eventDetailsDTO.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getPastEvents(view.context)
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Displays the selected event deletion dialog
     */
    fun showDeleteClubEventDialog(view: View, user: EventDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.event_delete_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteEvent(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}