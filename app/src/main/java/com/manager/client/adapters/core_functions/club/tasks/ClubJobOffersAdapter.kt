package com.manager.client.adapters.core_functions.club.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.jobs.JobOfferListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.ClubJobOfferDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.CLUB
import com.manager.client.utils.COMMA
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.club_manage_job_offers_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubJobOffersAdapter : RecyclerView.Adapter<ClubJobOffersAdapter.ClubJobOffersViewHolder>() {

    var clubJobOffers: ArrayList<JobOfferDetailsDTO> = ArrayList()

    init {
        getAllClubJobOffers()
    }

    class ClubJobOffersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubJobOffersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_manage_job_offers_list_item, parent, false)

        return ClubJobOffersViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubJobOffersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.clubNameLogo.text = clubJobOffers[position].clubName[0].toString()
        holder.view.profession.text = clubJobOffers[position].profession.plus(COMMA)
        holder.view.experience.text = clubJobOffers[position].experience
        holder.view.location.text = BULLET_DOT.plus(clubJobOffers[position].location)

        holder.view.clubNameLogo.setOnClickListener { getClubJobOfferDetails(holder.view, clubJobOffers[position]) }
        holder.view.profession.setOnClickListener { getClubJobOfferDetails(holder.view, clubJobOffers[position]) }
        holder.view.location.setOnClickListener { getClubJobOfferDetails(holder.view, clubJobOffers[position]) }
        holder.view.experience.setOnClickListener { getClubJobOfferDetails(holder.view, clubJobOffers[position]) }
        holder.view.offerDeleteButton.setOnClickListener { showDeleteClubJobOfferDialog(holder.view, clubJobOffers[position]) }
    }

    override fun getItemCount() = clubJobOffers.size

    /**
     * Retrieves the job offers list of the current club from the database
     */
    fun getAllClubJobOffers() {

        clubJobOffers.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubJobOffersRequest = retrofitInstance?.getAllCurrentJobOffersByCategoryAndUser(
            CLUB)

        getAllClubJobOffersRequest?.enqueue(object : Callback<JobOfferListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferListDTO>?, apiResponseDTO: Response<JobOfferListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferList: JobOfferListDTO? = apiResponseDTO.body()
                    clubJobOffers = clubJobOfferList!!.jobOfferDetailsList as ArrayList<JobOfferDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected offer details from the database
     */
    fun getClubJobOfferDetails(view: View, clubJobOfferDetails: JobOfferDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getClubJobOfferDetails = retrofitInstance?.getSelectedJobOfferDetails(clubJobOfferDetails.id)
        getClubJobOfferDetails?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferDetailsDTO>?, apiResponseDTO: Response<JobOfferDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, ClubJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", true)
                    detailsIntent.putExtra("clubJobOfferDetails", clubJobOfferDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the club job offer from the database
     */
    fun deleteClubJobOffer(view: View, clubJobOffer: JobOfferDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteJobOffer(clubJobOffer.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllClubJobOffers()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected offer deletion dialog
     */
    fun showDeleteClubJobOfferDialog(view: View, user: JobOfferDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.club_job_offer_delete_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteClubJobOffer(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}