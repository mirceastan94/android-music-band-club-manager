package com.manager.client.adapters.core_functions.notifications

import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.main.NotificationDTO
import com.manager.client.models.main.NotificationListDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.NotificationDetailsActivity
import com.manager.client.utils.SELECTED_LANGUAGE
import kotlinx.android.synthetic.main.notifications_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationsAdapter(applicationContext: Context) : RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder>() {

    var allNotifications: ArrayList<NotificationDTO> = ArrayList()

    val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
    val chosenLanguage = preferences.getString(SELECTED_LANGUAGE, "")

    init {
        getNotifications(applicationContext)
    }

    class NotificationsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationsViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.notifications_list_item, parent, false)

        return NotificationsViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: NotificationsViewHolder, position: Int) {
        // sets notification information within the list
        holder.view.notificationNameLogo.text = allNotifications[position].subjectType[0].toString()

        when (chosenLanguage) {
            "en" -> {
                if (allNotifications[position].textEnglish.length > 70) {
                    holder.view.notificationDescription.text = allNotifications[position].textEnglish.subSequence(0, 70).replaceRange(68, 70, "...")
                } else {
                    holder.view.notificationDescription.text = allNotifications[position].textEnglish
                }
            }
            "de" -> {
                if (allNotifications[position].textGerman.length > 70) {
                    holder.view.notificationDescription.text = allNotifications[position].textGerman.subSequence(0, 70).replaceRange(68, 70, "...")
                } else {
                    holder.view.notificationDescription.text = allNotifications[position].textGerman
                }
            }
        }

        holder.view.notificationNameLogo.setOnClickListener { getNotificationDetails(holder.view, allNotifications[position]) }
        holder.view.notificationDescription.setOnClickListener { getNotificationDetails(holder.view, allNotifications[position]) }
        holder.view.notificationInfoButton.setOnClickListener { getNotificationDetails(holder.view, allNotifications[position]) }
    }

    override fun getItemCount() = allNotifications.size

    /**
     * Retrieves the notifications list
     */
    fun getNotifications(context: Context) {

        allNotifications.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllNotifications = retrofitInstance?.getNotifications()

        getAllNotifications?.enqueue(object : Callback<NotificationListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<NotificationListDTO>?, apiResponseDTO: Response<NotificationListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val notificationlistDTO: NotificationListDTO? = apiResponseDTO.body()
                    allNotifications = notificationlistDTO!!.notificationList as ArrayList<NotificationDTO>
                    if (allNotifications.isEmpty()) {
                        var refreshToast = Toast.makeText(context, R.string.notifications_not_found_error_text, Toast.LENGTH_LONG)
                        refreshToast.setGravity(Gravity.BOTTOM, 0, 40)
                        refreshToast.show()
                    }
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<NotificationListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected notification details from the database
     */
    fun getNotificationDetails(view: View, notification: NotificationDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedNotificationDetails(notification.id)
        getUserDetailsDTO?.enqueue(object : Callback<NotificationDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<NotificationDTO>?, apiResponseDTO: Response<NotificationDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val notificationDTO: NotificationDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, NotificationDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("notificationDetails", notificationDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<NotificationDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}