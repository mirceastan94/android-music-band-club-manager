package com.manager.client.adapters.core_functions.club.general

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.jobs.JobOfferListDTO
import com.manager.client.models.jobs.JobOfferSearchDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.ClubJobOfferDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.CLUB
import com.manager.client.utils.COMMA
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.club_join_job_offers_list_item.view.*
import kotlinx.android.synthetic.main.club_manage_job_offers_list_item.view.clubNameLogo
import kotlinx.android.synthetic.main.club_manage_job_offers_list_item.view.experience
import kotlinx.android.synthetic.main.club_manage_job_offers_list_item.view.location
import kotlinx.android.synthetic.main.club_manage_job_offers_list_item.view.profession
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubJoinJobOffersAdapter(clubJobOfferSearchDTO: JobOfferSearchDTO?, context: Context) : RecyclerView.Adapter<ClubJoinJobOffersAdapter.ClubJobOffersViewHolder>() {

    var allClubJobOffers: ArrayList<JobOfferDetailsDTO> = ArrayList()


    init {
        getAllClubJobOffers(clubJobOfferSearchDTO, context)
    }

    class ClubJobOffersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubJobOffersViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_join_job_offers_list_item, parent, false)

        return ClubJobOffersViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: ClubJobOffersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.clubNameLogo.text = allClubJobOffers[position].clubName[0].toString()
        holder.view.profession.text = allClubJobOffers[position].profession.plus(COMMA)
        holder.view.experience.text = allClubJobOffers[position].experience
        holder.view.location.text = BULLET_DOT.plus(allClubJobOffers[position].location)

        holder.view.clubNameLogo.setOnClickListener { getClubJobOfferDetails(holder.view, allClubJobOffers[position]) }
        holder.view.profession.setOnClickListener { getClubJobOfferDetails(holder.view, allClubJobOffers[position]) }
        holder.view.location.setOnClickListener { getClubJobOfferDetails(holder.view, allClubJobOffers[position]) }
        holder.view.experience.setOnClickListener { getClubJobOfferDetails(holder.view, allClubJobOffers[position]) }
        holder.view.offerInfoButton.setOnClickListener { getClubJobOfferDetails(holder.view, allClubJobOffers[position]) }
    }

    override fun getItemCount() = allClubJobOffers.size

    /**
     * Retrieves the job offers list for the specific clubs in the database
     */
    fun getAllClubJobOffers(clubJobOfferSearchDTO: JobOfferSearchDTO?, context: Context) {

        allClubJobOffers.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        var getAllClubJobOffersRequest: Call<JobOfferListDTO>?
        when (clubJobOfferSearchDTO) {
            null -> getAllClubJobOffersRequest = retrofitInstance?.getAllJobOffersByCategory(CLUB)
            else -> getAllClubJobOffersRequest = retrofitInstance?.getCustomJobOffers(clubJobOfferSearchDTO)
        }
        getAllClubJobOffersRequest?.enqueue(object : Callback<JobOfferListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferListDTO>?, apiResponseDTO: Response<JobOfferListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferList: JobOfferListDTO? = apiResponseDTO.body()
                    allClubJobOffers = clubJobOfferList!!.jobOfferDetailsList as ArrayList<JobOfferDetailsDTO>
                    if (allClubJobOffers.isEmpty()) {
                        Toast.makeText(context, R.string.club_job_offer_no_suitable_clubs_found_error_text, Toast.LENGTH_LONG).show()
                    }
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    if (errorResponse.message.contains("no criteria found", true)) {
                        Toast.makeText(context, R.string.club_job_offer_no_criteria_chosen_error_text, Toast.LENGTH_LONG).show()
                        MediaPlayerManager.play(context, R.raw.sound_warning)
                    }
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected offer details from the database
     */
    fun getClubJobOfferDetails(view: View, user: JobOfferDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedJobOfferDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferDetailsDTO>?, apiResponseDTO: Response<JobOfferDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, ClubJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", false)
                    detailsIntent.putExtra("clubJobOfferDetails", clubJobOfferDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}