package com.manager.client.adapters.core_functions.club.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubMemberListDTO
import com.manager.client.models.club.entities.ClubMemberSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.UserDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.USER_WITH_ADMIN_ROLE_DELETE_CONSTRAINT
import kotlinx.android.synthetic.main.club_manage_members_list_item.view.*
import kotlinx.android.synthetic.main.club_member_list_item.view.name
import kotlinx.android.synthetic.main.club_member_list_item.view.notificationNameLogo
import kotlinx.android.synthetic.main.club_member_list_item.view.profession
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubManageMembersAdapter : RecyclerView.Adapter<ClubManageMembersAdapter.ClubMembersViewHolder>() {

    var users: ArrayList<ClubMemberSummary> = ArrayList()

    init {
        getAllClubMembers()
    }

    class ClubMembersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubMembersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_manage_members_list_item, parent, false)

        return ClubMembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubMembersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 20) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 21).replaceRange(19, 21, "...")
        }
        holder.view.profession.text = BULLET_DOT.plus(users[position].profession)

        holder.view.notificationNameLogo.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.name.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.profession.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.memberDeleteButton.setOnClickListener { showDeleteUserDialog(holder.view, users[position]) }
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list from the database
     */
    fun getAllClubMembers() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubMembersRequest = retrofitInstance?.getMyClubMembers()

        getAllClubMembersRequest?.enqueue(object : Callback<ClubMemberListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubMemberListDTO>?, apiResponseDTO: Response<ClubMemberListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: ClubMemberListDTO? = apiResponseDTO.body()
                    users = userList!!.memberSummaryList as ArrayList<ClubMemberSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubMemberListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected user details from the database
     */
    fun getUserDetails(view: View, user: ClubMemberSummary) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedUserDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponseDTO: Response<UserDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userDetails: UserDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, UserDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("UserDetails", userDetails?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the user from the database
     */
    fun deleteUser(view: View, user: ClubMemberSummary) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteClubMember(user.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                    if (errorResponse.message.contains(USER_WITH_ADMIN_ROLE_DELETE_CONSTRAINT)) {
                        Toast.makeText(view.context, R.string.alert_delete_admin_users_text, Toast.LENGTH_SHORT).show()
                    }
                    MediaPlayerManager.play(view.context, R.raw.sound_error);
                }
                getAllClubMembers()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected user deletion dialog
     */
    fun showDeleteUserDialog(view: View, user: ClubMemberSummary) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.club_manage_members_remove_member_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteUser(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}