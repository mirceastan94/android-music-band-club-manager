package com.manager.client.adapters.core_functions.club.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubMemberListDTO
import com.manager.client.models.club.entities.ClubMemberSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.MainActivity
import com.manager.client.ui.activities.UserDetailsActivity
import com.manager.client.utils.ActivityHelper
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.club_change_owner_members_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubChangeOwnerAdapter : RecyclerView.Adapter<ClubChangeOwnerAdapter.ClubMembersViewHolder>() {

    var users: ArrayList<ClubMemberSummary> = ArrayList()

    init {
        getAllClubMembersExceptTheCurrentOwner()
    }

    class ClubMembersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubMembersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_change_owner_members_list_item, parent, false)

        return ClubMembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubMembersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 12) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 13).replaceRange(11, 13, "...")
        }
        holder.view.profession.text = BULLET_DOT.plus(users[position].profession)

        holder.view.notificationNameLogo.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.name.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.profession.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.ownerChangeButton.setOnClickListener { showChangeOwnerDialog(holder.view, users[position]) }
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list from the database, except for the current owner
     */
    fun getAllClubMembersExceptTheCurrentOwner() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubMembersRequest = retrofitInstance?.getAllClubMembersExceptTheCurrentOwner()

        getAllClubMembersRequest?.enqueue(object : Callback<ClubMemberListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubMemberListDTO>?, apiResponseDTO: Response<ClubMemberListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: ClubMemberListDTO? = apiResponseDTO.body()
                    users = userList!!.memberSummaryList as ArrayList<ClubMemberSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubMemberListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected user details from the database
     */
    fun getUserDetails(view: View, user: ClubMemberSummary) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedUserDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponseDTO: Response<UserDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userDetails: UserDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, UserDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("UserDetails", userDetails?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Changes the club owner
     */
    fun changeOwner(view: View, user: ClubMemberSummary) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.changeClubOwner(user.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    ActivityHelper.startActivity(view.context, MainActivity::class.java)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected user change owner dialog
     */
    fun showChangeOwnerDialog(view: View, user: ClubMemberSummary) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.club_change_owner_update_owner_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    changeOwner(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}