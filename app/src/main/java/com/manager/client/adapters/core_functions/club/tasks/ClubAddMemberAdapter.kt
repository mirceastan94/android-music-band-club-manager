package com.manager.client.adapters.core_functions.club.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubMemberListDTO
import com.manager.client.models.club.entities.ClubMemberSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.UserDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.club_member_list_item.view.name
import kotlinx.android.synthetic.main.club_member_list_item.view.notificationNameLogo
import kotlinx.android.synthetic.main.club_users_without_club_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubAddMemberAdapter : RecyclerView.Adapter<ClubAddMemberAdapter.ClubAddMemberViewHolder>() {

    var users: ArrayList<ClubMemberSummary> = ArrayList()

    init {
        getAllMembersWithoutClub()
    }

    class ClubAddMemberViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubAddMemberViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_users_without_club_list_item, parent, false)

        return ClubAddMemberViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubAddMemberViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 20) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 21).replaceRange(19, 21, "...")
        }
        holder.view.details.text = BULLET_DOT.plus(users[position].profession.plus(", ").plus(users[position].location))

        holder.view.notificationNameLogo.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.name.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.details.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.memberAddButton.setOnClickListener { showAddUserDialog(holder.view, users[position]) }
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list without club from the database
     */
    fun getAllMembersWithoutClub() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubMembersRequest = retrofitInstance?.getAllMembersWithoutClub()

        getAllClubMembersRequest?.enqueue(object : Callback<ClubMemberListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubMemberListDTO>?, apiResponseDTO: Response<ClubMemberListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: ClubMemberListDTO? = apiResponseDTO.body()
                    users = userList!!.memberSummaryList as ArrayList<ClubMemberSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubMemberListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected user details from the database
     */
    fun getUserDetails(view: View, user: ClubMemberSummary) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedUserDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponseDTO: Response<UserDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userDetails: UserDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, UserDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("UserDetails", userDetails?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Adds the user to the club
     */
    fun addMemberUser(view: View, user: ClubMemberSummary) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.addClubMember(user.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllMembersWithoutClub()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Displays the selected user add dialog
     */
    fun showAddUserDialog(view: View, user: ClubMemberSummary) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.club_manage_members_add_member_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    addMemberUser(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}