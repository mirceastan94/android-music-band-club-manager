package com.manager.client.adapters.user_management.admin

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.band.dto.BandDetailsDTO
import com.manager.client.models.band.dto.BandListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.BandDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.bands_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandsDashboardAdapter : RecyclerView.Adapter<BandsDashboardAdapter.BandsViewHolder>() {

    var bands: ArrayList<BandDetailsDTO> = ArrayList()

    init {
        getAllBands()
    }

    class BandsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BandsDashboardAdapter.BandsViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.bands_list_item, parent, false)

        return BandsViewHolder(view)
    }

    override fun onBindViewHolder(holder: BandsViewHolder, position: Int) {
        // sets bands information within the list
        holder.view.notificationNameLogo.text = bands[position].name[0].toString()
        holder.view.name.text = bands[position].name
        holder.view.genre.text = BULLET_DOT.plus(bands[position].genre)

        // calls the logic responsible methods for retrieving the selected band details and deletion
        holder.view.notificationNameLogo.setOnClickListener { getBandDetails(holder.view, bands[position]) }
        holder.view.name.setOnClickListener { getBandDetails(holder.view, bands[position]) }
        holder.view.genre.setOnClickListener { getBandDetails(holder.view, bands[position]) }
        holder.view.deleteButton.setOnClickListener { showDeleteBandDialog(holder.view, bands[position]) }
    }

    override fun getItemCount() = bands.size

    /**
     * Retrieves the band list from the database
     */
    fun getAllBands() {

        bands.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllBandsRequest = retrofitInstance?.getAllBands()

        getAllBandsRequest?.enqueue(object : Callback<BandListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<BandListDTO>?, apiResponseDTO: Response<BandListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandList: BandListDTO? = apiResponseDTO.body()
                    bands = bandList!!.bandSummaryList as ArrayList<BandDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<BandListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected band details from the database
     */
    fun getBandDetails(view: View, band: BandDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedBandDetails(band.name)
        getUserDetailsDTO?.enqueue(object : Callback<BandDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<BandDetailsDTO>?, apiResponseDTO: Response<BandDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandDetailsDTO: BandDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, BandDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("bandDetails", bandDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<BandDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the band from the database
     */
    fun deleteBand(view: View, band: BandDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteBand(band.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllBands()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Displays the selected band deletion dialog
     */
    fun showDeleteBandDialog(view: View, band: BandDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.alert_delete_band_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteBand(view, band)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}