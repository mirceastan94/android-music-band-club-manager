package com.manager.client.adapters.user_management.admin

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.EventDetailsDTO
import com.manager.client.models.events.dto.EventListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.EventsDetailsActivity
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.events_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventsDashboardAdapter : RecyclerView.Adapter<EventsDashboardAdapter.EventsViewHolder>() {

    var events: ArrayList<EventDetailsDTO> = ArrayList()

    init {
        getAllEvents()
    }

    class EventsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventsDashboardAdapter.EventsViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.events_list_item, parent, false)

        return EventsViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        // sets clubs information within the list
        holder.view.clubNameLogo.text = events[position].clubName[0].toString()
        holder.view.clubName.text = events[position].clubName.plus(COMMA)
        holder.view.bandName.text = events[position].bandName
        holder.view.location.text = BULLET_DOT.plus(events[position].location)
        holder.view.dateTime.text = ARROW_WITH_SPACE.plus(events[position].dateTime.replace(T, COMMA))

        holder.view.clubNameLogo.setOnClickListener { getEventDetails(holder.view, events[position]) }
        holder.view.clubName.setOnClickListener { getEventDetails(holder.view, events[position]) }
        holder.view.bandName.setOnClickListener { getEventDetails(holder.view, events[position]) }
        holder.view.location.setOnClickListener { getEventDetails(holder.view, events[position]) }
        holder.view.dateTime.setOnClickListener { getEventDetails(holder.view, events[position]) }
        holder.view.eventRemoveButton.setOnClickListener { showDeleteClubDialog(holder.view, events[position]) }
    }

    override fun getItemCount() = events.size

    /**
     * Retrieves the event list from the database
     */
    fun getAllEvents() {

        events.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubsRequest = retrofitInstance?.getAllEvents()

        getAllClubsRequest?.enqueue(object : Callback<EventListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventListDTO>?, apiResponseDTO: Response<EventListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubList: EventListDTO? = apiResponseDTO.body()
                    events = clubList!!.eventSummaryList as ArrayList<EventDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected event details from the database
     */
    fun getEventDetails(view: View, event: EventDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getSelectedClubDetailsDTO = retrofitInstance?.getSelectedEventDetails(event.id)
        getSelectedClubDetailsDTO?.enqueue(object : Callback<EventDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventDetailsDTO>?, apiResponseDTO: Response<EventDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventDetailsDTO: EventDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, EventsDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("eventDetails", eventDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the event from the database
     */
    fun deleteEvent(view: View, event: EventDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteEvent(event.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllEvents()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Displays the selected club deletion dialog
     */
    fun showDeleteClubDialog(view: View, event: EventDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.alert_delete_event_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteEvent(view, event)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}