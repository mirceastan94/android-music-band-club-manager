package com.manager.client.adapters.core_functions.events

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.ReviewDetailsDTO
import com.manager.client.models.events.dto.ReviewListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.COMMA
import com.manager.client.utils.QUOTES
import kotlinx.android.synthetic.main.event_view_feedback_list_item.view.*
import kotlinx.android.synthetic.main.event_view_participant_list_item.view.*
import kotlinx.android.synthetic.main.event_view_participant_list_item.view.location
import kotlinx.android.synthetic.main.event_view_participant_list_item.view.name
import kotlinx.android.synthetic.main.event_view_participant_list_item.view.nameLogo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventFeedbackListAdapter(eventId: String) : RecyclerView.Adapter<EventFeedbackListAdapter.EventDetailsViewHolder>() {

    var eventFeedbackList: ArrayList<ReviewDetailsDTO> = ArrayList()
    val eventId = eventId

    init {
        getEventFeedbackList()
    }

    class EventDetailsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventDetailsViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.event_view_feedback_list_item, parent, false)

        return EventDetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventDetailsViewHolder, position: Int) {
        // sets users information within the list
        holder.view.nameLogo.text = eventFeedbackList[position].userName[0].toString()
        holder.view.name.text = eventFeedbackList[position].userName.plus(COMMA)
        holder.view.location.text = eventFeedbackList[position].userLocation
        holder.view.feedback.text = QUOTES.plus(eventFeedbackList[position].eventFeedback).plus(QUOTES)
    }

    override fun getItemCount() = eventFeedbackList.size

    /**
     * Retrieves the users list from the database
     */
    fun getEventFeedbackList() {

        eventFeedbackList.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllEventParticipants = retrofitInstance?.getEventFeedbackList(eventId.toLong())

        getAllEventParticipants?.enqueue(object : Callback<ReviewListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ReviewListDTO>?, apiResponseDTO: Response<ReviewListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventFeedbackList: ReviewListDTO? = apiResponseDTO.body()
                    this@EventFeedbackListAdapter.eventFeedbackList = eventFeedbackList!!.reviewSummaryList as ArrayList<ReviewDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ReviewListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}