package com.manager.client.adapters.core_functions.band.general

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.manager.client.adapters.core_functions.band.BandCreateFragment
import com.manager.client.adapters.core_functions.band.BandDeleteFragment
import com.manager.client.adapters.core_functions.band.BandOverviewFragment
import com.manager.client.adapters.core_functions.band.BandUpdateFragment

class BandManagementGeneralAdapter(val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // sets the correct fragment based on the selected item
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return BandCreateFragment()
            }
            1 -> {
                return BandUpdateFragment()
            }
            2 -> {
                return BandOverviewFragment()
            }
            3 -> {
                return BandDeleteFragment()
            }
            else -> return null
        }
    }

    // returns the total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}