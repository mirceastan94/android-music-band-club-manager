package com.manager.client.adapters.user_management.admin

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.models.users.dto.UserDetailsDTO
import com.manager.client.models.users.dto.UserListDTO
import com.manager.client.models.users.entities.UserSummary
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.UserDetailsActivity
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.users_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UsersDashboardAdapter : RecyclerView.Adapter<UsersDashboardAdapter.UsersViewHolder>() {

    var users: ArrayList<UserSummary> = ArrayList()

    init {
        getAllUsers()
    }

    class UsersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UsersDashboardAdapter.UsersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.users_list_item, parent, false)

        return UsersViewHolder(view)
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 12) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 13).replaceRange(11, 13, "...")
        }
        holder.view.profession.text = BULLET_DOT.plus(users[position].profession)
        if (users[position].role.toLowerCase().contains(ADMIN)) {
            holder.view.userLabel.setAlpha(0.20.toFloat())
            holder.view.adminLabel.setAlpha(1.toFloat())
        } else {
            holder.view.adminLabel.setAlpha(0.20.toFloat())
            holder.view.userLabel.setAlpha(1.toFloat())
        }

        // calls the logic responsible methods for retrieving the selected user details, role change and deletion
        holder.view.notificationNameLogo.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.name.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.profession.setOnClickListener { getUserDetails(holder.view, users[position]) }
        holder.view.adminLabel.setOnClickListener { showUpdateUserRoleDialog(holder.view, ADMIN_ROLE, users[position]) }
        holder.view.userLabel.setOnClickListener { showUpdateUserRoleDialog(holder.view, USER_ROLE, users[position]) }
        holder.view.deleteButton.setOnClickListener { showDeleteUserDialog(holder.view, users[position]) }
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list from the database
     */
    fun getAllUsers() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllUsersRequest = retrofitInstance?.getAllUsers()

        getAllUsersRequest?.enqueue(object : Callback<UserListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserListDTO>?, apiResponseDTO: Response<UserListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: UserListDTO? = apiResponseDTO.body()
                    users = userList!!.userSummaryList as ArrayList<UserSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected user details from the database
     */
    fun getUserDetails(view: View, user: UserSummary) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedUserDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<UserDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<UserDetailsDTO>?, apiResponseDTO: Response<UserDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userDetails: UserDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, UserDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("UserDetails", userDetails?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<UserDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Edits the user role
     */
    fun editUserRole(view: View, role: String, user: UserSummary) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.editUserRole(user.id, role)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                    if (errorResponse.message.contains(MASTER_USER_CHANGE_ROLE_CONSTRAINT)) {
                        Toast.makeText(view.context, R.string.alert_edit_master_user_role_text, Toast.LENGTH_SHORT).show()
                    }
                    MediaPlayerManager.play(view.context, R.raw.sound_error);
                }
                getAllUsers()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the user from the database
     */
    fun deleteUser(view: View, user: UserSummary) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteUser(user.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                    if (errorResponse.message.contains(USER_WITH_ADMIN_ROLE_DELETE_CONSTRAINT)) {
                        Toast.makeText(view.context, R.string.alert_delete_admin_users_text, Toast.LENGTH_SHORT).show()
                    }
                    MediaPlayerManager.play(view.context, R.raw.sound_error)
                }
                getAllUsers()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected user role update dialog
     */
    fun showUpdateUserRoleDialog(view: View, role: String, user: UserSummary) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.alert_edit_user_role_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    editUserRole(view, role, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

    /**
     * Displays the selected user deletion dialog
     */
    fun showDeleteUserDialog(view: View, user: UserSummary) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.alert_delete_user_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteUser(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}