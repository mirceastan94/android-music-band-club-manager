package com.manager.client.adapters.core_functions.band.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.jobs.JobOfferListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.BandJobOfferDetailsActivity
import com.manager.client.utils.BAND
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.COMMA
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.band_manage_job_offers_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandJobOffersAdapter : RecyclerView.Adapter<BandJobOffersAdapter.BandJobOffersViewHolder>() {

    var bandJobOffers: ArrayList<JobOfferDetailsDTO> = ArrayList()

    init {
        getAllBandJobOffers()
    }

    class BandJobOffersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BandJobOffersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.band_manage_job_offers_list_item, parent, false)

        return BandJobOffersViewHolder(view)
    }

    override fun onBindViewHolder(holder: BandJobOffersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.bandNameLogo.text = bandJobOffers[position].bandName[0].toString()
        holder.view.profession.text = bandJobOffers[position].profession.plus(COMMA)
        holder.view.experience.text = bandJobOffers[position].experience
        holder.view.location.text = BULLET_DOT.plus(bandJobOffers[position].location)

        holder.view.bandNameLogo.setOnClickListener {
            getBandJobOfferDetails(
                holder.view,
                bandJobOffers[position]
            )
        }
        holder.view.profession.setOnClickListener {
            getBandJobOfferDetails(
                holder.view,
                bandJobOffers[position]
            )
        }
        holder.view.location.setOnClickListener {
            getBandJobOfferDetails(
                holder.view,
                bandJobOffers[position]
            )
        }
        holder.view.experience.setOnClickListener {
            getBandJobOfferDetails(
                holder.view,
                bandJobOffers[position]
            )
        }
        holder.view.offerDeleteButton.setOnClickListener {
            showDeleteBandJobOfferDialog(
                holder.view,
                bandJobOffers[position]
            )
        }
    }

    override fun getItemCount() = bandJobOffers.size

    /**
     * Retrieves the job offers list of the current band from the database
     */
    fun getAllBandJobOffers() {

        bandJobOffers.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getAllBandJobOffersRequest = retrofitInstance?.getAllCurrentJobOffersByCategoryAndUser(
            BAND
        )

        getAllBandJobOffersRequest?.enqueue(object : Callback<JobOfferListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<JobOfferListDTO>?,
                apiResponseDTO: Response<JobOfferListDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandJobOfferList: JobOfferListDTO? = apiResponseDTO.body()
                    bandJobOffers =
                        bandJobOfferList!!.jobOfferDetailsList as ArrayList<JobOfferDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected offer details from the database
     */
    fun getBandJobOfferDetails(view: View, bandJobOfferDetails: JobOfferDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val getBandJobOfferDetails =
            retrofitInstance?.getSelectedJobOfferDetails(bandJobOfferDetails.id)
        getBandJobOfferDetails?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<JobOfferDetailsDTO>?,
                apiResponseDTO: Response<JobOfferDetailsDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent =
                        Intent(view.context, BandJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", true)
                    detailsIntent.putExtra(
                        "bandJobOfferDetails",
                        bandJobOfferDetailsDTO?.toBundle()
                    )
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the band job offer from the database
     */
    fun deleteBandJobOffer(view: View, bandJobOffer: JobOfferDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }
                ?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteJobOffer(bandJobOffer.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(
                call: Call<APIResponseDTO>?,
                apiResponseDTO: Response<APIResponseDTO>?
            ) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllBandJobOffers()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected offer deletion dialog
     */
    fun showDeleteBandJobOfferDialog(view: View, user: JobOfferDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.band_job_offer_delete_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteBandJobOffer(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}