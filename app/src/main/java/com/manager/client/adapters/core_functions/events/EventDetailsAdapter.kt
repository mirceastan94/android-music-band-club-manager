package com.manager.client.adapters.core_functions.events

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.EventParticipantListDTO
import com.manager.client.models.events.entities.EventParticipantSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BULLET_DOT
import kotlinx.android.synthetic.main.event_view_participant_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventDetailsAdapter(eventId: String) : RecyclerView.Adapter<EventDetailsAdapter.EventDetailsViewHolder>() {

    var eventParticipants: ArrayList<EventParticipantSummary> = ArrayList()
    val eventId = eventId

    init {
        getAllEventParticipants()
    }

    class EventDetailsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventDetailsViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.event_view_participant_list_item, parent, false)

        return EventDetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: EventDetailsViewHolder, position: Int) {
        // sets users information within the list
        holder.view.nameLogo.text = eventParticipants[position].name[0].toString()
        if (eventParticipants[position].name.length <= 20) {
            holder.view.name.text = eventParticipants[position].name
        } else {
            holder.view.name.text = eventParticipants[position].name.subSequence(0, 21).replaceRange(19, 21, "...")
        }
        holder.view.location.text = BULLET_DOT.plus(eventParticipants[position].location)
    }

    override fun getItemCount() = eventParticipants.size

    /**
     * Retrieves the users list from the database
     */
    fun getAllEventParticipants() {

        eventParticipants.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllEventParticipants = retrofitInstance?.getEventParticipants(eventId.toLong())

        getAllEventParticipants?.enqueue(object : Callback<EventParticipantListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventParticipantListDTO>?, apiResponseDTO: Response<EventParticipantListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventParticipantList: EventParticipantListDTO? = apiResponseDTO.body()
                    eventParticipants = eventParticipantList!!.eventParticipantList as ArrayList<EventParticipantSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventParticipantListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}