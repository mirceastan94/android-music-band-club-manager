package com.manager.client.adapters.core_functions.club

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ClubManagementMainAdapter(val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // sets the correct fragment based on the selected item
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return ClubGeneralMainFragment()
            }
            1 -> {
                return ClubTasksMainFragment()
            }
            else -> return null
        }
    }

    // returns the total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}