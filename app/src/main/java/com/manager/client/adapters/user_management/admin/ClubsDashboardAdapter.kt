package com.manager.client.adapters.user_management.admin

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubDetailsDTO
import com.manager.client.models.club.dto.ClubListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.ClubDetailsActivity
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.clubs_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubsDashboardAdapter : RecyclerView.Adapter<ClubsDashboardAdapter.ClubsViewHolder>() {

    var clubs: ArrayList<ClubDetailsDTO> = ArrayList()

    init {
        getAllClubs()
    }

    class ClubsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubsDashboardAdapter.ClubsViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.clubs_list_item, parent, false)

        return ClubsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubsViewHolder, position: Int) {
        // sets clubs information within the list
        holder.view.notificationNameLogo.text = clubs[position].name[0].toString()
        holder.view.name.text = clubs[position].name
        holder.view.genre.text = BULLET_DOT.plus(clubs[position].genre)

        // calls the logic responsible methods for retrieving the selected club details and deletion
        holder.view.notificationNameLogo.setOnClickListener { getClubDetails(holder.view, clubs[position]) }
        holder.view.name.setOnClickListener { getClubDetails(holder.view, clubs[position]) }
        holder.view.genre.setOnClickListener { getClubDetails(holder.view, clubs[position]) }
        holder.view.deleteButton.setOnClickListener { showDeleteClubDialog(holder.view, clubs[position]) }
    }

    override fun getItemCount() = clubs.size

    /**
     * Retrieves the club list from the database
     */
    fun getAllClubs() {

        clubs.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubsRequest = retrofitInstance?.getAllClubs()

        getAllClubsRequest?.enqueue(object : Callback<ClubListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubListDTO>?, apiResponseDTO: Response<ClubListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubList: ClubListDTO? = apiResponseDTO.body()
                    clubs = clubList!!.clubSummaryList as ArrayList<ClubDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })

    }

    /**
     * Retrieves the selected club details from the database
     */
    fun getClubDetails(view: View, club: ClubDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getSelectedClubDetailsDTO = retrofitInstance?.getSelectedClubDetails(club.name)
        getSelectedClubDetailsDTO?.enqueue(object : Callback<ClubDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubDetailsDTO>?, apiResponseDTO: Response<ClubDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubDetailsDTO: ClubDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, ClubDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("clubDetails", clubDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the club from the database
     */
    fun deleteClub(view: View, club: ClubDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteClub(club.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllClubs()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Displays the selected club deletion dialog
     */
    fun showDeleteClubDialog(view: View, club: ClubDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.alert_delete_club_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteClub(view, club)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}