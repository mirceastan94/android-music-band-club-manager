package com.manager.client.adapters.core_functions.band.tasks

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.manager.client.adapters.core_functions.band.BandChangeOwnerFragment
import com.manager.client.adapters.core_functions.band.BandJobOffersFragment
import com.manager.client.adapters.core_functions.band.BandManageMembersFragment

class BandManagementTasksAdapter(val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // sets the correct fragment based on the selected item
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return BandManageMembersFragment()
            }
            1 -> {
                return BandChangeOwnerFragment()
            }
            2 -> {
                return BandJobOffersFragment()
            }
            else -> return null
        }
    }

    // returns the total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}