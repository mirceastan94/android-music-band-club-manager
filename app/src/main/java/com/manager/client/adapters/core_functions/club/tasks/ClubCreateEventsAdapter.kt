package com.manager.client.adapters.core_functions.club.tasks

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.EventDetailsDTO
import com.manager.client.models.events.dto.EventListDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.EventsDetailsActivity
import com.manager.client.utils.COMMA
import com.manager.client.utils.MediaPlayerManager
import com.manager.client.utils.T
import kotlinx.android.synthetic.main.club_manage_events_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubCreateEventsAdapter : RecyclerView.Adapter<ClubCreateEventsAdapter.ClubEventsViewHolder>() {

    var clubEvents: ArrayList<EventDetailsDTO> = ArrayList()

    init {
        getAllClubEvents()
    }

    class ClubEventsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubEventsViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_manage_events_list_item, parent, false)

        return ClubEventsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubEventsViewHolder, position: Int) {
        // sets users information within the list
        holder.view.bandNameLogo.text = clubEvents[position].bandName[0].toString()
        holder.view.bandName.text = clubEvents[position].bandName
        holder.view.dateTime.text = clubEvents[position].dateTime.replace(T, COMMA)

        holder.view.bandNameLogo.setOnClickListener { getEventDetails(holder.view, clubEvents[position]) }
        holder.view.bandName.setOnClickListener { getEventDetails(holder.view, clubEvents[position]) }
        holder.view.dateTime.setOnClickListener { getEventDetails(holder.view, clubEvents[position]) }
        holder.view.eventDeleteButton.setOnClickListener { showDeleteClubEventDialog(holder.view, clubEvents[position]) }
    }

    override fun getItemCount() = clubEvents.size

    /**
     * Retrieves the events list of the current club from the database
     */
    fun getAllClubEvents() {

        clubEvents.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubEventsRequest = retrofitInstance?.getClubEvents()

        getAllClubEventsRequest?.enqueue(object : Callback<EventListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventListDTO>?, apiResponseDTO: Response<EventListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubEventList: EventListDTO? = apiResponseDTO.body()
                    clubEvents = clubEventList!!.eventSummaryList as ArrayList<EventDetailsDTO>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected event details from the database
     */
    fun getEventDetails(view: View, clubEventDetails: EventDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getClubEventDetails = retrofitInstance?.getSelectedEventDetails(clubEventDetails.id)
        getClubEventDetails?.enqueue(object : Callback<EventDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventDetailsDTO>?, apiResponseDTO: Response<EventDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventDetails: EventDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, EventsDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOver", false)
                    detailsIntent.putExtra("eventDetails", eventDetails?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Deletes the club event from the database
     */
    fun deleteClubEvent(view: View, clubEventDetails: EventDetailsDTO) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.deleteEvent(clubEventDetails.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getAllClubEvents()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }


    /**
     * Displays the selected event deletion dialog
     */
    fun showDeleteClubEventDialog(view: View, user: EventDetailsDTO) {
        MediaPlayerManager.play(view.context, R.raw.sound_warning)
        val alertBuilder = AlertDialog.Builder(view.context, R.style.AlertDialogTheme)
        alertBuilder.setMessage(R.string.event_delete_alert_text).setCancelable(true)
            .setPositiveButton(R.string.alert_yes_text,
                DialogInterface.OnClickListener { _, _ ->
                    deleteClubEvent(view, user)
                }).setNegativeButton(R.string.alert_no_text,
                DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        val alert = alertBuilder.create()
        alert.show()
    }

}