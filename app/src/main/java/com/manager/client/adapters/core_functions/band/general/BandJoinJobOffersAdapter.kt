package com.manager.client.adapters.core_functions.band.general

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.jobs.JobOfferDetailsDTO
import com.manager.client.models.jobs.JobOfferListDTO
import com.manager.client.models.jobs.JobOfferSearchDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.BandJobOfferDetailsActivity
import com.manager.client.utils.BAND
import com.manager.client.utils.BULLET_DOT
import com.manager.client.utils.COMMA
import com.manager.client.utils.MediaPlayerManager
import kotlinx.android.synthetic.main.band_join_job_offers_list_item.view.*
import kotlinx.android.synthetic.main.band_manage_job_offers_list_item.view.bandNameLogo
import kotlinx.android.synthetic.main.band_manage_job_offers_list_item.view.experience
import kotlinx.android.synthetic.main.band_manage_job_offers_list_item.view.location
import kotlinx.android.synthetic.main.band_manage_job_offers_list_item.view.profession
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BandJoinJobOffersAdapter(bandJobOfferSearchDTO: JobOfferSearchDTO?, context: Context) : RecyclerView.Adapter<BandJoinJobOffersAdapter.BandJobOffersViewHolder>() {

    var allBandJobOffers: ArrayList<JobOfferDetailsDTO> = ArrayList()

    init {
        getAllBandJobOffers(bandJobOfferSearchDTO, context)
    }

    class BandJobOffersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BandJobOffersViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.band_join_job_offers_list_item, parent, false)

        return BandJobOffersViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: BandJobOffersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.bandNameLogo.text = allBandJobOffers[position].bandName[0].toString()
        holder.view.profession.text = allBandJobOffers[position].profession.plus(COMMA)
        holder.view.experience.text = allBandJobOffers[position].experience
        holder.view.location.text = BULLET_DOT.plus(allBandJobOffers[position].location)

        holder.view.bandNameLogo.setOnClickListener { getBandJobOfferDetails(holder.view, allBandJobOffers[position]) }
        holder.view.profession.setOnClickListener { getBandJobOfferDetails(holder.view, allBandJobOffers[position]) }
        holder.view.location.setOnClickListener { getBandJobOfferDetails(holder.view, allBandJobOffers[position]) }
        holder.view.experience.setOnClickListener { getBandJobOfferDetails(holder.view, allBandJobOffers[position]) }
        holder.view.offerInfoButton.setOnClickListener { getBandJobOfferDetails(holder.view, allBandJobOffers[position]) }
    }

    override fun getItemCount() = allBandJobOffers.size

    /**
     * Retrieves the job offers list for the specific bands in the database
     */
    fun getAllBandJobOffers(bandJobOfferSearchDTO: JobOfferSearchDTO?, context: Context) {

        allBandJobOffers.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        var getAllBandJobOffersRequest: Call<JobOfferListDTO>?
        when (bandJobOfferSearchDTO) {
            null -> getAllBandJobOffersRequest = retrofitInstance?.getAllJobOffersByCategory(BAND)
            else -> getAllBandJobOffersRequest = retrofitInstance?.getCustomJobOffers(bandJobOfferSearchDTO)
        }
        getAllBandJobOffersRequest?.enqueue(object : Callback<JobOfferListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferListDTO>?, apiResponseDTO: Response<JobOfferListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandJobOfferList: JobOfferListDTO? = apiResponseDTO.body()
                    allBandJobOffers = bandJobOfferList!!.jobOfferDetailsList as ArrayList<JobOfferDetailsDTO>
                    if (allBandJobOffers.isEmpty()) {
                        Toast.makeText(context, R.string.band_job_offer_no_suitable_bands_found_error_text, Toast.LENGTH_LONG).show()
                    }
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    if (errorResponse.message.contains("no criteria found", true)) {
                        Toast.makeText(context, R.string.band_job_offer_no_criteria_chosen_error_text, Toast.LENGTH_LONG).show()
                        MediaPlayerManager.play(context, R.raw.sound_warning)
                    }
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected offer details from the database
     */
    fun getBandJobOfferDetails(view: View, user: JobOfferDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedJobOfferDetails(user.id)
        getUserDetailsDTO?.enqueue(object : Callback<JobOfferDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<JobOfferDetailsDTO>?, apiResponseDTO: Response<JobOfferDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val bandJobOfferDetailsDTO: JobOfferDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, BandJobOfferDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOwner", false)
                    detailsIntent.putExtra("bandJobOfferDetails", bandJobOfferDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson();
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<JobOfferDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}