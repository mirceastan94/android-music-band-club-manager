package com.manager.client.adapters.core_functions.club.general

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.club.dto.ClubMemberListDTO
import com.manager.client.models.club.entities.ClubMemberSummary
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.utils.BULLET_DOT
import kotlinx.android.synthetic.main.club_member_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClubOverviewMembersAdapter : RecyclerView.Adapter<ClubOverviewMembersAdapter.ClubMembersViewHolder>() {

    var users: ArrayList<ClubMemberSummary> = ArrayList()

    init {
        getAllClubMembers()
    }

    class ClubMembersViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClubMembersViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.club_member_list_item, parent, false)

        return ClubMembersViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClubMembersViewHolder, position: Int) {
        // sets users information within the list
        holder.view.notificationNameLogo.text = users[position].name[0].toString()
        if (users[position].name.length <= 12) {
            holder.view.name.text = users[position].name
        } else {
            holder.view.name.text = users[position].name.subSequence(0, 13).replaceRange(11, 13, "...")
        }
        holder.view.profession.text = BULLET_DOT.plus(users[position].profession)
    }

    override fun getItemCount() = users.size

    /**
     * Retrieves the users list from the database
     */
    fun getAllClubMembers() {

        users.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getAllClubMembersRequest = retrofitInstance?.getMyClubMembers()

        getAllClubMembersRequest?.enqueue(object : Callback<ClubMemberListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<ClubMemberListDTO>?, apiResponseDTO: Response<ClubMemberListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val userList: ClubMemberListDTO? = apiResponseDTO.body()
                    users = userList!!.memberSummaryList as ArrayList<ClubMemberSummary>
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<ClubMemberListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }
}