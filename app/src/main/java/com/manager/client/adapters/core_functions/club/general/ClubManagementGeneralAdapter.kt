package com.manager.client.adapters.core_functions.club.general

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.manager.client.adapters.core_functions.club.ClubCreateFragment
import com.manager.client.adapters.core_functions.club.ClubDeleteFragment
import com.manager.client.adapters.core_functions.club.ClubOverviewFragment
import com.manager.client.adapters.core_functions.club.ClubUpdateFragment

class ClubManagementGeneralAdapter(val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // sets the correct fragment based on the selected item
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return ClubCreateFragment()
            }
            1 -> {
                return ClubUpdateFragment()
            }
            2 -> {
                return ClubOverviewFragment()
            }
            3 -> {
                return ClubDeleteFragment()
            }
            else -> return null
        }
    }

    // returns the total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}