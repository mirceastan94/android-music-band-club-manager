package com.manager.client.adapters.core_functions.club.tasks

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.manager.client.adapters.core_functions.club.ClubChangeOwnerFragment
import com.manager.client.adapters.core_functions.club.ClubCreateEventsFragment
import com.manager.client.adapters.core_functions.club.ClubJobOffersFragment
import com.manager.client.adapters.core_functions.club.ClubManageMembersFragment

class ClubManagementTasksAdapter(val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // sets the correct fragment based on the selected item
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> {
                return ClubManageMembersFragment()
            }
            1 -> {
                return ClubChangeOwnerFragment()
            }
            2 -> {
                return ClubJobOffersFragment()
            }
            3 -> {
                return ClubCreateEventsFragment()
            }
            else -> return null
        }
    }

    // returns the total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}