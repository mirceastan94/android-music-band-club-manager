package com.manager.client.adapters.core_functions.events

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.gson.Gson
import com.manager.client.R
import com.manager.client.models.events.dto.EventDetailsDTO
import com.manager.client.models.events.dto.EventListDTO
import com.manager.client.models.events.dto.EventSearchDTO
import com.manager.client.models.main.APIResponseDTO
import com.manager.client.network.JWTRetrofit
import com.manager.client.network.ManagerAPI
import com.manager.client.network.accessTokenProvidingInterceptor
import com.manager.client.network.makeRetrofit
import com.manager.client.ui.activities.EventsDetailsActivity
import com.manager.client.utils.*
import kotlinx.android.synthetic.main.events_join_list_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventsJoinAdapter(eventSearchDTO: EventSearchDTO?, context: Context) : RecyclerView.Adapter<EventsJoinAdapter.EventsViewHolder>() {

    var allEvents: ArrayList<EventDetailsDTO> = ArrayList()
    var eventSearch: EventSearchDTO? = null

    init {
        getCustomEvents(eventSearchDTO, context)
    }

    class EventsViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EventsViewHolder {

        val rootView = LayoutInflater.from(parent.context)
            .inflate(R.layout.events_join_list_item, parent, false)

        return EventsViewHolder(rootView)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        // sets events information within the list
        holder.view.clubNameLogo.text = allEvents[position].clubName[0].toString()
        holder.view.clubName.text = allEvents[position].clubName.plus(COMMA)
        holder.view.bandName.text = allEvents[position].bandName
        holder.view.location.text = BULLET_DOT.plus(allEvents[position].location)
        holder.view.dateTime.text = ARROW_WITH_SPACE.plus(allEvents[position].dateTime.replace(T, COMMA))

        holder.view.clubNameLogo.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.clubName.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.bandName.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.location.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.dateTime.setOnClickListener { getEventDetails(holder.view, allEvents[position]) }
        holder.view.eventAddButton.setOnClickListener { addEvent(holder.view, allEvents[position], eventSearch) }
    }

    override fun getItemCount() = allEvents.size

    /**
     * Retrieves the events list for the specific criteria in the database
     */
    fun getCustomEvents(eventSearchDTO: EventSearchDTO?, context: Context) {
        eventSearch = eventSearchDTO

        allEvents.clear()

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        var getAllEventsRequest: Call<EventListDTO>?
        when (eventSearchDTO) {
            null -> getAllEventsRequest = retrofitInstance?.getAllUpcomingEvents()
            else -> getAllEventsRequest = retrofitInstance?.getCustomCurrentEvents(eventSearchDTO)
        }
        getAllEventsRequest?.enqueue(object : Callback<EventListDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventListDTO>?, apiResponseDTO: Response<EventListDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val clubJobOfferList: EventListDTO? = apiResponseDTO.body()
                    allEvents = clubJobOfferList!!.eventSummaryList as ArrayList<EventDetailsDTO>
                    if (allEvents.isEmpty()) {
                        Toast.makeText(context, R.string.event_join_no_suitable_events_found_error_text, Toast.LENGTH_LONG).show()
                    }
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    if (errorResponse.message.contains("no criteria found", true)) {
                        Toast.makeText(context, R.string.club_job_offer_no_criteria_chosen_error_text, Toast.LENGTH_LONG).show()
                        MediaPlayerManager.play(context, R.raw.sound_warning)
                    }
                    Log.e("Call response", errorResponse.message)
                }
                notifyDataSetChanged()
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventListDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Retrieves the selected event details from the database
     */
    fun getEventDetails(view: View, event: EventDetailsDTO) {
        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val getUserDetailsDTO = retrofitInstance?.getSelectedEventDetails(event.id)
        getUserDetailsDTO?.enqueue(object : Callback<EventDetailsDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<EventDetailsDTO>?, apiResponseDTO: Response<EventDetailsDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                    val eventDetailsDTO: EventDetailsDTO? = apiResponseDTO.body()
                    val detailsIntent = Intent(view.context, EventsDetailsActivity::class.java)
                    detailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    detailsIntent.putExtra("isOver", false)
                    detailsIntent.putExtra("eventDetails", eventDetailsDTO?.toBundle())
                    view.context.startActivity(detailsIntent)
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<EventDetailsDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

    /**
     * Adds the event to the user's list in the database
     */
    fun addEvent(view: View, eventDetailsDTO: EventDetailsDTO, eventSearchDTO: EventSearchDTO?) {

        val retrofitInstance =
            JWTRetrofit.let { makeRetrofit(accessTokenProvidingInterceptor()) }?.create(ManagerAPI::class.java)
        val apiResponseDTO = retrofitInstance?.addEventParticipant(eventDetailsDTO.id)
        apiResponseDTO?.enqueue(object : Callback<APIResponseDTO> {

            /**
             * Handles the call response
             */
            override fun onResponse(call: Call<APIResponseDTO>?, apiResponseDTO: Response<APIResponseDTO>?) {
                if (apiResponseDTO != null && apiResponseDTO.isSuccessful) {
                    Log.v(apiResponseDTO.toString(), "")
                } else {
                    val gson = Gson()
                    val adapter = gson.getAdapter(APIResponseDTO::class.java)
                    val errorResponse = adapter.fromJson(apiResponseDTO?.errorBody()?.string())
                    Log.e("Call response", errorResponse.message)
                }
                getCustomEvents(eventSearchDTO, view.context)
            }

            /**
             * Handles the call failure
             */
            override fun onFailure(call: Call<APIResponseDTO>?, throwable: Throwable?) {
                Log.e("Error caught: ", throwable.toString())
            }
        })
    }

}