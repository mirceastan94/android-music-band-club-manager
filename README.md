Music Band & Club Manager is, as the title suggests, a manager application created for the Android ecosystem, in 2019, in which every user is capable of creating a personalised network, tailored for her or his selection criteria.

The business motivation for this application was the lack of such applications published on the Play Store currently, although a lot of people desire to take part into the music networking world with minimal effort, in a centralised solution, where “Music” is the core keyword in every possible activity.
Once the application is loaded, the user is given a set of options after the language choice, register and login processes, in which she or he can opt to utilise the following three main functions:

• create a new band or/and a club and gain total control over it/them, from the genre this entity is built for, the location where this entity will activate for the most part, the members that will be added to the entity, the ownership assignment onto a different user, to the job offers published to gather new members and so on;

• join a band or/and a club owned by another human being, in which this user becomes a member with a dedicated position within the band/club;

• attend events created by other club owners, in which a band will attempt to capture the headlights through a top notch concert.

Moreover, the application features a notification feed view in the main menu, where any user can instantly see news regarding bands, clubs, events and even users, allowing them to easily check out when and where a specific event will occur, and send emails to band or club owners to negotiate their future, after watching what these entities are all about.

Various settings functionalities are also in place from the moment one is logged in for the first time, the user being able to change her/his account details, open the smartphone’s Gmail app to check her/his job offer emails, delete her/his account provided the user is not the "master" one, or get total access to other user roles along with the posibility of removing them from the database altogether, provided the logged in user has an admin role at the moment of accessing the admin dashboard.